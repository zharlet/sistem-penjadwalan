@extends('layouts.base')
@section('content')   
@include('varviewjs')
<style>
	.table-striped{
		border-spacing: 0 .1em!important;
	}
	.tr-custom>.th-custom{
		color:white;

	}
	.tr-custom>.td-custom{
		background-color: #dbdbdb;
		
	}
	.tr-custom>.th-custom:nth-child(odd){
		background-color: #324960;
	}
	.tr-custom>.th-custom:nth-child(even){
		background-color: #4FC3A1;
	}
	.tr-custom>.td-custom{

		border-color: none;
	}
	.fc-event{
		height: 27px!important;
	}
	.fc-nonbusiness {
        border-style: none!important;
        opacity: 1;
        background-color: #324960!important;
    }
	.fc-button {
        background: #324960;
        background-image: none;
        color: white;
        border-color: white;
        border-bottom-color: white;
    }
    th.fc-day-header, th.fc-widget-header, th.fc-mon, th.fc-past, th>a{
        background-color: #324960;
        color:white;
        height: 40px;
        vertical-align: middle;
    }
    tr{
        height: 30px;
    }
</style>
<div class="row">
	<div class="col-md-7">
		<div class="panel panel-default">
           
            <div class="panel-body" id="calendarForm">
                {!! $calendar->calendar() !!}
            </div>
        </div>
	</div>

	<div class="col-md-5">
		<div class="box">
            <div class="box-header">
              <h3 class="box-title">@lang('appointment.title')</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="list_app" class="table table-bordered table-striped">
                <thead>
                <tr class="tr-custom">
                  <th class="th-custom">@lang('appointment.table.name')</th>
                  <th class="th-custom">@lang('appointment.table.date')</th>
                  <th class="th-custom">@lang('appointment.table.time')</th>
                  <th class="th-custom">@lang('appointment.table.name')</th>
                  <th class="th-custom">Status</th>
                  <th class="th-custom">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($appointment as $value)
                	<tr class="tr-custom">
	                  <td class="td-custom" valign="middle" align="center">{{$value['Name']}}</td>
	                  <td class="td-custom" valign="middle" align="center">{{$value['Date']}}</td>
	                  <td class="td-custom" valign="middle" align="center">{{$value['Time']}}</td>
	                  <td class="td-custom" valign="middle" align="center">{{$value['Place']}}</td>
	                  @if($value['Status'] == "Active")
	                  	<td class="td-custom" valign="middle" align="center" style="color: #6BA5C1; font-weight: 700;">                	
	                		{{$value['Status']}}
	              	  	</td>
	              	  @elseif($value['Status'] == "Cancel")
	              	  	<td class="td-custom" valign="middle" align="center" style="color:#EF7A7A;  font-weight: 700;">                	
	                		{{$value['Status']}}
	              	  	</td>
	              	  @elseif($value['Status'] == "Waiting")
	              	  	<td class="td-custom" valign="middle" align="center" style="color: #cf70f4; font-weight: 700;">                	
	                		{{$value['Status']}}
	              	  	</td>
	              	  @endif
	                  <td class="td-custom" valign="middle" align="center">
	                  @if($value['Status'] == 'Active')
		                  	<div class="btn-group">
		                      <button style="padding: 5px" type="button" class="btn btn-default cancel" id="{{$value['ID']}}" data-toggle="modal" data-target="#cancel-form">Cancel</button>
		                      <button style="padding: 5px" id="{{$value['ID']}}" type="button" data-toggle="modal" data-target="#rsch-form" class="btn btn-default reschedule">Re-Schedule</button>
		                    </div>
	                    @else
	                    	<p><i>No Action Needed</i></p>
	                    @endif
	                  </td>
	                </tr>
                @endforeach    
                </tbody>
              	</table>
              	<div class="modal fade" id="rsch-form">
		          	<div class="modal-dialog">
		            	<div class="modal-content">
			              	<div class="modal-header">
			                	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                  	<span aria-hidden="true">&times;</span></button>
			                	<h4 class="modal-title">Reschedule Appointment</h4>
			            	</div>
			         		<form>
			         			<div class="modal-body">
		                			<div class="panel panel-default">
				               			<div class='input-group date'>
								  			<input type='text' id="dateResch" class="form-control datetimepicker" />
								  			<span class="input-group-addon datetimepicker-addon">
								    			<span class="glyphicon glyphicon-calendar"></span>
								  			</span>
										</div>
			              			</div>
				            		<div class="modal-footer">
				                		<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
				                		<button type="submit" id="submitResch" class="btn btn-primary">Apply Recomended</button>
				              		</div>
				            	</div>
			         		</form>
		          		</div>
		       		</div>
            	</div>
            	<div  class="modal modal-warning fade" id="cancel-form">
		          <div class="modal-dialog" style="width:25%">
		            <div class="modal-content">
		              <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                  <span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title">Confirm Your Action</h4>
		              </div>
		              <div class="modal-body">
		                <p>Are you sure want to cancel this appointment ?</p>
		              </div>
		              <div class="modal-footer" >
		                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">No</button>
		                <button type="button" id="submitCancel" class="btn btn-outline">Yes</button>
		              </div>
		            </div>
		            <!-- /.modal-content -->
		          </div>
        		</div>
          	</div>
		</div>
</div>
<script>
	$(function () {
		var idUpdate = "";

		if(myval.lang == 'in'){
			$('#list_app').dataTable({
		    "columnDefs" : [{"targets":1, "type":"date-eu"}],
		    "bInfo": true,
		    "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Indonesian.json"
            }
    		});
		}else{
			$('#list_app').dataTable({
		    "columnDefs" : [{"targets":1, "type":"date-eu"}],
		    "bInfo": true
    	});
		}
    	
    	$('.reschedule, .cancel').on('click',function(){
    		idUpdate = $(this).attr('id');
    	})
    	$('.datetimepicker').datetimepicker({
    		format: 'Y-MM-DDTHH:mm:ss+07:00'
    	});
	  	$('.datetimepicker-addon').on('click', function() {
		  	$(this).prev('input.datetimepicker').data('DateTimePicker').toggle();
		});
		$("#submitResch").on("click",function(event){
			event.preventDefault();
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
			var dateRsch = $("#dateResch").val();
			$.ajax({
                url: '/appointment/reschedule/callback',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    id: idUpdate,
                    date: dateRsch
                },
                dataType: 'JSON',
                success: function (data) { 
                    location.reload();
                }
            });  
		});

		$("#submitCancel").on("click",function(event){
			event.preventDefault();
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
			$.ajax({
                url: '/appointment/cancel/callback',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    id: idUpdate
                },
                dataType: 'JSON',
                success: function (data) { 
                    location.reload();
                }
            });  
		})  
  	})
</script>
 {!! $calendar->script() !!}

 @endsection