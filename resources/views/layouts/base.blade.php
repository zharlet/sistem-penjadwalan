<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>
                Aplikasi Penjadwalan Tatap Muka
            </title>
        <link href="{{asset('dist/css/main.css')}}" rel="stylesheet"></link>
        
        <link src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.print.css"></link>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.css"></link>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
        <link rel="stylesheet" href="{{asset('dist/bower_components/font-awesome/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('dist/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
        
        <!-- Ionicons -->
        <link rel="stylesheet" href="{{asset('dist/bower_components/Ionicons/css/ionicons.min.css')}}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{asset('dist/css/AdminLTE.min.css')}}">
        <!-- iCheck -->
        <link rel="stylesheet" href="{{asset('dist/plugins/iCheck/square/blue.css')}}">
        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap2-toggle.min.css" rel="stylesheet"></link>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
        <link href="https://fonts.googleapis.com/css?family=PT+Sans|Open+Sans" rel="stylesheet">
        @if(isset($themeCalendar))
            @if($themeCalendar == "flat")
                <link rel="stylesheet" type="text/css" href="{{asset('dist/css/calendar/flat.css')}}">
            @endif
            @if($themeCalendar == "classic")
                <link rel="stylesheet" type="text/css" href="{{asset('dist/css/calendar/classic.css')}}">
            @endif
        @endif
        <!-- load jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

        <!--Script-->

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
        <script src="{{asset('dist/js/moment.min.js')}}"></script>
        <script src="{{asset('dist/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('dist/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.js"></script>
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap2-toggle.min.js"></script>
        <script src="https://cdn.datatables.net/plug-ins/1.10.11/sorting/date-eu.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>



       <style>
        .bg-primary {
            color: #fff !important;
            background-color: #3dc379 !important;

        }
        .btn-primary {
            color: #fff;
            background-color: #3dc379!important;
            border-color: white;
        }
       </style>

    </head>
    <body>
        <nav class="navbar navbar-inverse" style="border-radius: 0px;">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span>Menu</span>
                    </button>
                    <a  class="navbar-brand" href="/"><span class="glyphicon glyphicon-home"></span> &nbsp; SCHEDULER SYSTEM</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        @if(Auth::user()->user_unique == "Dosen")
                            <li id="nav-setting"><a href="/setting"><span class="glyphicon glyphicon-wrench"></span>@lang('base.setting_menu')</a></li>
                            <li id="nav-applist"><a href="/appointment/check"><span class="glyphicon glyphicon-tags"></span> &nbsp;@lang('base.check_menu')</a></li>
                            <li id="nav-schedule"><a href="/schedule/manage"><span class="glyphicon glyphicon-calendar">@lang('base.sch_menu')</span></a></li>
                        @else
                            
                            <li id="nav-setting"><a href="/setting"><span class="glyphicon glyphicon-wrench"></span> Setting</a></li>
                            <li id="nav-createapp" ><a data-toggle="modal" data-target="#search-form" ><span class="glyphicon glyphicon-plus"></span> Create App</a></li>
                            <li title="Check Appointment" id="nav-applist"><a href="/appointment/list"> <span class="glyphicon glyphicon-tags"></span> &nbsp; Check App</a></li>
                        @endif
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a class="isDisabled"><span class="glyphicon glyphicon-user"></span> {{ Auth::user()->name }} </a></li>
                        <li><a href="{{ url('/logout') }}"> <span class="glyphicon glyphicon-log-out"></span> @lang('base.logout_menu')
                          </a>
                        </li>
                        @if(\Session::get('locale') == 'in')
                            <li>
                                <a href="{{ url('/locale/en') }}">EN</a>
                                <script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/locale/id.js'></script>
                            </li>
                        @else
                            <li>
                                <a href="{{ url('/locale/in') }}">IN</a>
                            </li>
                        @endif
                        
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form> 

                    </ul>
                </div>
            </div>
        </nav>
        <!-- /navbar -->
        <!-- Main component for call to action -->
        <div class="modal fade" id="search-form">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Serach Username Lecturer</h4>
                    </div>
                    <form>
                        <div class="modal-body">
                            <div class="panel panel-default">
                                <div class='input-group date'>
                                    <input type='text' id="uname_sch" class="form-control" onchange="check()" />
                                    <span class="input-group-addon">
                                        <span onclick="check()" class="glyphicon glyphicon-search"></span>
                                    </span>
                                </div>
                                
                            </div>
                            <div class="respond-report">
                                <center><p id="report"></p></center>                               
                            </div>
                            <div class="modal-footer">
                                <!-- <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button> -->
                                <button type="submit" id="apply_uname" class="btn btn-primary" disabled>Apply</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @if(isset($pageSpec))
         <div class="container-fluid">
            @yield('content')
        </div>
    @else
        <div class="container">
            @yield('content')
        </div>
    @endif
       
    </div>
    <!-- /container -->

    </body>
    <script>
        function check(){
                var uname_sch = $("#uname_sch").val();
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/search/lecturer?uname_sch='+uname_sch,
                    type: 'GET',
                    data: {
                        _token: CSRF_TOKEN
                    },
                    dataType: 'JSON',
                    success: function (data) { 
                        console.log(data);
                        if(data['status'] == "Username Available"){
                            $("#report").html(data['status'] + ". Name Is " + data['name']);
                            $(".respond-report").append('<center><img id="temp-photo" src="'+data['photo']+'" alt="" width="250px"></center>')
                            $("#apply_uname").removeAttr('disabled');
                            
                        }else{
                            $("#report").html(data['status']);
                            $("#apply_uname").attr('disabled','true');
                            $("#temp-photo").remove();
                        }
                        
                    }
                });
                $("#apply_uname").on('click',function(e){
                    e.preventDefault();
                    window.location.href = "/appointment/create/"+uname_sch;
                }); 
        }
        var pathname = window.location.pathname;
        switch(true){
            case pathname.includes("/appointment/list"):
                $("#nav-applist").attr("class","active");
                break;
            case pathname == "/home" :
                $("#nav-home").attr("class","active");
                break;
            case pathname == "/" :
                $("#nav-home").attr("class","active");
                break;
            case pathname.includes("/appointment/create/") :
                $("#nav-createapp").attr("class","active");
                break;
            case pathname.includes("/schedule") :
                $("#nav-schedule").attr("class","active");
                break;
            case pathname.includes("/appointment/check"):
                $("#nav-applist").attr("class","active");
                break;
            case pathname.includes("/setting"):
                $("#nav-setting").attr("class","active");
                break;
        }
    </script>
</html>
