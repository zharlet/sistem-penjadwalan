
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>@yield('title')</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<header>
		<nav>
			<a href="/home">Home</a>
			<a href="/api/gcalendar/create">Scheduler</a>
			<a href="/">Setting</a>
		</nav>
	</header><!-- /header -->
	<br>

	@yield('content')

	<br>
	<footer>
		<p>
			&copy; Zharlet | Sistem Penjadwalan 2019 Filkom UB 
		</p>
	</footer>
</body>
</html>