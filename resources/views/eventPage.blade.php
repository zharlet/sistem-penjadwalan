@extends('layouts.base')
@section('content') 
<style>
table{
	border-spacing: 0 .1em!important;
}
tr>th{
	color:white;

}
tr>td{
	background-color: #dbdbdb;
	
}
tr>th:nth-child(odd){
	background-color: #324960;
}
tr>th:nth-child(even){
	background-color: #4FC3A1;
}
tr>td{
	border:0!important;
	border-color: none;
}
</style>
<script>
   //  $(document).ready(function(){
   //      var at = null;
   //      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   //      $.ajax({
   //          url: '/ajax/getAccessToken/',
   //          type: 'POST',
   //          data: {
   //              _token: CSRF_TOKEN
   //          },
   //          dataType: 'JSON',
   //          success: function (data) { 
   //              console.log("Success");
   //          }
   //      });  
   // });    
</script>
<div class="col-md-12">
	<div class="box">
	    <div class="box-header">
	      <h2 style='font-family: "Trebuchet MS", Tahoma, sans-serif;'>Your Appointment</h2>
	       <hr>
	    </div>
	    <!-- /.box-header -->
	   
	    <div class="box-body">
	      <table id="list_app" class="table table-bordered table-striped">
	        <thead>
	        <tr>
	          <th>Lecturer</th>
	          <th>Date</th>
	          <th>Time</th>
	          <th>Place</th>
	          <th>Status</th>
	          <th>Action</th>
	        </tr>
	        </thead>
	        <tbody>
	        @foreach($events as $value)
	        	<tr>
	              <td valign="center" align="center">{{$value['Name']}}</td>
	              <td valign="center" align="center">{{$value['Date']}}</td>
	              <td valign="center" align="center">{{$value['Time']}}</td>
	              <td valign="center" align="center">{{$value['Place']}}</td>
	              <td valign="center" align="center">{{$value['Status']}}</td>
	              <td valign="center" align="center">
	              	@if($value['OriStatus'] == 'Waiting')
	              		<div class="btn-group">
		                  <button style="padding: 5px" type="button" class="btn btn-default deny" id="{{$value['ID']}}">Deny</button>
		                  <button style="padding: 5px" id="{{$value['ID']}}" type="button" class="btn btn-default accept">Accept</button>
		                </div>
	              	@else
	              		<p><i>No Action Needed</i></p>
	              	@endif
	              	
	              </td>
	            </tr>
	        @endforeach    
	        </tbody>
	      	</table>
	  	</div>
	</div>
</div>
<script>
	$(function(){
		var idUpdate = "";
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		$('#list_app').dataTable({
		    "columnDefs" : [{"targets":1, "type":"date-eu"}],
		    "bInfo": true
    	});
    	$('.deny').on('click',function(e){
    		e.preventDefault();
    		idUpdate = $(this).attr('id');
			$.ajax({
                url: '/reschedule/deny/callback',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    id: idUpdate
                },
                dataType: 'JSON',
                success: function (data) { 
                    location.reload();
                }
            })
    	})
    	$('.accept').on('click',function(e){
    		e.preventDefault();
    		idUpdate = $(this).attr('id');
    		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	        $.ajax({
	            url: '/ajax/getAccessToken/',
	            type: 'POST',
	            data: {
	                _token: CSRF_TOKEN,
	                id: idUpdate,
	                action: "reschedule"
	            },
	            dataType: 'JSON',
	            success: function (data) { 
	                $.ajax({
		                url: '/reschedule/accept/callback',
		                type: 'POST',
		                data: {
		                    _token: CSRF_TOKEN,
		                    id: idUpdate
		                },
		                dataType: 'JSON',
		                success: function (data) { 
		                    location.reload();
		                }
		            })
	            }
	        });  
			
    	})


	})
	
</script>
@endsection