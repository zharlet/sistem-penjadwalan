@extends('layouts.base')

@section('content')
<!-- Main component for call to action -->
<style>
  a, a:hover{
    color: white;
  }
</style>
<div class="jumbotron">
    @if(Auth::user()->user_unique == "Dosen")
            <h1>
                @lang('setting.title')
            </h1>
            <p>
               @lang('setting.description')
            </p>
            <hr>

        <form class="form-horizontal" action="/setting/save" method="POST" accept-charset="utf-8">
          {{csrf_field()}}
            <div class="form-group">
                  <label  class="col-sm-2 control-label">@lang('setting.input.ua')</label>
                  <div class="col-sm-10">
                    <input type="text" name="uname_access" class="form-control" id="inputEmail3" placeholder="Username Access" value="{{ $setting->username_sch }}" required>
                  </div>
                </div>
            <div class="form-group">
              <label class="col-sm-2 control-label" >@lang('setting.input.name')</label>
              <div class="col-sm-10">
                <input type="text" name="name" class="form-control" id="inputEmail3" placeholder="Name" value="{{ $setting->name }}" required>
              </div>
            </div>
       
            <div class="form-group">
              <label class="col-sm-2 control-label">Email</label>
              <div class="col-sm-10">
                <div class="input-group margin" style="margin-left: 0px; margin-top: 0px;">
                <input type="email" class="form-control" id="inputEmail3" placeholder="Email" value="{{ $setting->email }}" disabled>
                <span class="input-group-btn">
          
                    <button id="changeEmail" type="submit" class="btn btn-info btn-flat">Change!</button>
                </span>
              </div>
              </div>
            </div>
    
            <div class="form-group">
              <label class="col-sm-2 control-label">@lang('setting.input.password')</label>
              <div class="col-sm-10">
                <div class="input-group margin" style="margin-left: 0px; margin-top: 0px;">
                    <input type="password" class="form-control" id="inputEmail3" value="***" disabled>
                    <span class="input-group-btn">
                      <button data-toggle="modal" data-target="#password-form" type="button" class="btn btn-info btn-flat">Change!</button>
                    </span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Status</label>
              <div class="col-sm-10">
                <div class="switch">
                  <label>
                    @if($setting->isOnline == 1)
                        <input name="isOnline" class="chcksts" type="checkbox" checked >
                        <span class="lever">Online</span>
                    @else
                        <input name="isOnline" class="chcksts" type="checkbox">
                        <span class="lever">Offline</span>
                    @endif
                  </label>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Google Calendar</label>
              <div class="col-sm-10">
                @if($bool == "true")
                    <a href="{{ url('/createapp') }}" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-calendar"></i> Register using Google+</a>
                    <label>@lang('setting.input.gcal_null')</label>
                @else
                 <div class="input-group margin" style="margin-left: 0px; margin-top: 0px;">
                    <input type="email" class="form-control" value="{{ $setting->gcalendar }}" disabled>
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-info btn-flat"><a href="{{url('/createapp')}}">Change!</a></button>
                    </span>
                </div>
                    <label>@lang('setting.input.gcal_connected')</label>
                @endif
                
              </div>
              <hr>
            </div>
            <hr>
            <div class="form-group">
              <div class="col-md-12" >
                  @if($report == "Setting Saved")
                        <center><p style="color:green; font-size: 13px"><b>Setting Saved</b></p>
                          <br>
                  @else
                        <center><p style="color:red; font-size: 13px"><b>{{$report}}</b></p>
                          <br>
                  @endif
                 <center><button style="width:50%!important;" type="submit" class="btn btn-block btn-success btn-flat">@lang('setting.input.save')</button></center>
                  
              </div>
            </div>
        </form>      
    @else
        <form class="form-horizontal" action="/setting/save" method="POST" accept-charset="utf-8">
          {{csrf_field()}}
          <h1>
                Setting Your Account
          </h1>
          <p>
             Change Your Detail..
          </p>
          <hr>
            <div class="form-group">
              <label class="col-sm-2 control-label" >Name</label>
              <div class="col-sm-10">
                <input type="text" name="name" class="form-control" id="inputEmail3" placeholder="Name" value="{{ $setting->name }}">
              </div>
            </div>
       
            <div class="form-group">
              <label class="col-sm-2 control-label">Email</label>
              <div class="col-sm-10">
                <div class="input-group margin" style="margin-left: 0px; margin-top: 0px;">
                <input type="email" class="form-control" id="inputEmail3" placeholder="Email" value="{{ $setting->email }}" disabled>
                <span class="input-group-btn">
                    <button id="changeEmail" type="submit" class="btn btn-info btn-flat">Change!</button>
                </span>
              </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Password</label>
              <div class="col-sm-10">
                <div class="input-group margin" style="margin-left: 0px; margin-top: 0px;">
                    <input type="password" class="form-control" id="inputEmail3" value="***" disabled>
                    <span class="input-group-btn">
                      <button data-toggle="modal" data-target="#password-form" type="button" class="btn btn-info btn-flat">Change!</button>
                    </span>
                </div>
              </div>
            </div>
            <hr>
            <div class="form-group">
              <div class="col-md-12" >
                  @if($report == null)
                        <center><p style="color:green; font-size: 13px"><b>Setting Saved</b></p>
                          <br>
                  @else
                        <center><p style="color:red; font-size: 13px"><b>{{$report}}</b></p>
                          <br>
                  @endif
                 <center><button style="width:50%!important;" type="submit" class="btn btn-block btn-success btn-flat">Save</button></center>
                 
              </div>
            </div>
        </form>
    @endif
   <div class="modal fade" id="password-form">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title">Change Password..</h4>
                      </div>
                      <form action="/password/change" method="POST">
                        {{csrf_field()}}
                          <div class="modal-body">
                              <div class="panel panel-default">
                                  <div class="has-feedback">
                                     <input class="form-control" id="password-update" type="password"  name="password" placeholder="password" required>
                                  </div>
                                  <div class="has-feedback">
                                    <input class="form-control" 
                                    id="password-confirm" type="password" name="password_confirmation" placeholder="Retype Password" required>
                                  </div>                              
                              </div>
                              <div>
                                  <p style="font-size:15px; color:red; font-weight: bold;" id="pass-report"></p>
                              </div>
                              <div class="modal-footer">
                                  <button type="submit" id="apply_password" class="btn btn-primary" disabled>Apply</button>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
</div>
<script>
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content')
    $(".chcksts").change(function(){
        if($(this).is(':checked')){
            $(".lever").text("Online");  
        }else{
            $(".lever").text("Offline");
        }
    });
    $("#changeEmail").on("click",function(e){
      e.preventDefault();
      $.ajax({
        url: '/setting/email',
        type: 'POST',
        data: {
            _token: CSRF_TOKEN,
        },
        dataType: 'JSON',
        success: function (data) { 
            window.location.href = "/auth/redirect/google";
        }
      });
    });
    
      $("#password-update").change(function(){
        if($("#password-update").val() != ""){
          if($(this).val() != $("#password-confirm").val()){
            $("#pass-report").html("password not matched").css("color","red");
             $("#apply_password").attr("disabled","disabled");
          }else{
              $("#pass-report").html("password matched").css("color","green");
              $("#apply_password").removeAttr("disabled");
          }
        }
      })
      $("#password-confirm").change(function(){
        if($(this).val() != $("#password-update").val()){
          $("#pass-report").html("password not matched").css("color","red");
          $("#apply_password").attr("disabled","disabled");
        }else{
           $("#pass-report").html("password matched").css("color","green");
           $("#apply_password").removeAttr("disabled");
        }
      })

    

</script>
<!-- /container -->
@endsection