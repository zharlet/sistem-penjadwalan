@extends('layouts.base')
@section('content')   
@include('varviewjs')
<style>
    .control-label{
        font-size: 18px;
    }
    .table-striped{
        border-spacing: 0 .1em!important;
    }
    .tr-custom>.th-custom{
        color:white;

    }
    .tr-custom>.td-custom{
        background-color: #dbdbdb;
        
    }
    .tr-custom>.th-custom:nth-child(odd){
        background-color: #324960;
    }
    .tr-custom>.th-custom:nth-child(even){
        background-color: #4FC3A1;
    }
    .tr-custom>.td-custom{

        border-color: none;
    }
   
    
    tr{
        height: 30px;
    }
    hr.vertical{ 
        width: 2px;
        
        border:none;
        height: 700px;
        background-color: #7c8096;
    }
    .edit{
        background-color: white;
    }
    .btn-warning {
        background-color: #5f6271!important;
        border-color: #5f6271!important;
    }
    .bg-teal {
        background-color: #549c87 !important;
        height: 25px!important;
    }
    .btn-group-sm>.btn, .btn-sm {
        padding: 2px 10px!important;
    }
  
    input[type="text"]{
        font-family: Segoe UI,sans-serif; 
        font-weight: 500;
        color: #333;
        width: 100%; 
        box-sizing: border-box; 
        letter-spacing: 1px;
    }

    .col-3{float: left; width: 100%; position: relative;} /* necessary to give position: relative to parent. */
 /*   input[type="text"]{font: 15px/24px "Lato", Arial, sans-serif; color: #333; width: 100%; box-sizing: border-box; letter-spacing: 1px;}*/
    .effect-7{border: 1px solid #ccc; padding: 7px 14px 9px; transition: 0.4s;  }

    .effect-7 ~ .focus-border:before,
    .effect-7 ~ .focus-border:after{content: ""; position: absolute; top: 0; left: 50%; width: 0; height: 2px; background-color: #4fc3a1; transition: 0.4s;}
    .effect-7 ~ .focus-border:after{top: auto; bottom: 0;}
    .effect-7 ~ .focus-border i:before,
    .effect-7 ~ .focus-border i:after{content: ""; position: absolute; top: 50%; left: 0; width: 2px; height: 0; background-color: #4fc3a1; transition: 0.6s;}
    .effect-7 ~ .focus-border i:after{left: auto; right: 0;}
    .effect-7:focus ~ .focus-border:before,
    .effect-7:focus ~ .focus-border:after{left: 0; width: 100%; transition: 0.4s;}
    .effect-7:focus ~ .focus-border i:before,
    .effect-7:focus ~ .focus-border i:after{top: 0; height: 100%; transition: 0.6s;}
</style>
    
    <div class="row">
        <div class="col-md-5" id="panelSetting">
           
            <div class="jumbotron edit">
 <h4>@lang('manageSchedule.title')</h4>
            <hr>
                <form action="/schedule/manage/save" method="post" accept-charset="utf-8">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                    <h4> @lang('manageSchedule.input.preset') </h4>
                            </div>
                            <div class="col-md-10">
                                <div class="input-group input-group-sm">
                                    <div class="input-group-btn">
                                      <button type="button" id="presetSelected" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">@lang('manageSchedule.input.create')
                                        <span class="fa fa-caret-down"></span></button>
                                      <ul class="dropdown-menu" id="preset-data">
                                        <li class="divider"></li>
                                        <li><a data-toggle="modal" data-target="#create-schedule">+ Create New</a></li>
                                      </ul>
                                    </div>
                                    <!-- /btn-group -->
                                    @if(isset($setting))
                                        <input type="text" id="editPreset" name="namePreset" placeholder="Presets" class="form-control" value="{{ $setting['name'] }}">
                                        <input type="hidden" name="idPreset" value="{{ $idPreset }}">
                                    @endif
                                  </div>
                            </div>
                           
                        </div>
                    </div>
                    <div class="form-group" style="padding: 10px;">
                        <div class="row">
                            <div class="box-header" style="margin-bottom:-20px!important;">
                              <h3 class="box-title">@lang('manageSchedule.input.stime')</h3>
                              <div class="box-tools pull-right">
                                <button type="button" class="btn bg-teal btn-sm" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fa fa-caret-down"></i>
                                </button>
                              </div>
                            </div>
                            <hr style="border-color: #4FC3A1;" style="">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div id="collapseOne" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingOne">
                                  <div class="panel-body">
                                    <div class="col-md-6">
                                        <div class="col-3">
                                            @if(isset($setting))
                                            <input type="text" class="form-control effect-7" id="minTime" placeholder="Mintime" value="{{ $setting['minTime'] }}" name="minTime" required>
                                            <span class="focus-border">
                                                <i></i>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="col-3">
                                            @if(isset($setting))
                                            <input type="text" class="form-control effect-7" id="maxTime" placeholder="Username Access" value="{{ $setting['maxTime'] }}" name="maxTime" required>
                                            <span class="focus-border">
                                                <i></i>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                  </div>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" style="padding: 10px;">
                        <div class="row">
                            <div class="box-header" style="margin-bottom:-20px!important;">
                              <h3 class="box-title">@lang('manageSchedule.input.dshow')</h3>
                              <div class="box-tools pull-right">
                                <button type="button" class="btn bg-teal btn-sm" data-toggle="collapse" data-parent="#accordion" href="#daysShow" aria-expanded="true" aria-controls="collapseOne"><i class="fa fa-caret-down"></i>
                                </button>
                              </div>
                            </div>
                            <hr style="border-color: #4FC3A1;" style="">
                           
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div id="daysShow" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingOne">
                                 @if(isset($setting))
                                  <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <input type="checkbox" id="day-0" name="day[0]" value="0" checked>@lang('manageSchedule.days.sun')
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-3">
                                                 <input type="text" class="effect-7" id="time-0" name="time[0]" required>
                                                  <span class="focus-border">
                                                        <i></i>
                                                    </span>
                                                </div>
                                            </div>  
                                            <div class="col-md-3">
                                                <div class="col-3">
                                                    <input placeholder="@lang('manageSchedule.placeholder.room')" type="text" class="effect-7" id="room-0" name="room[0]" required><br>
                                                    <span class="focus-border"><i></i></span>
                                                </div>
                                            </div>  
                                        </div>
                                        <br>  
                                        <div class="row">
                                            <div class="col-md-3">
                                                 <input type="checkbox" id="day-1" name="day[1]" value="1" checked>@lang('manageSchedule.days.mon')
                                            </div>
                                            <div class="col-md-6">
                                                 <div class="col-3">
                                                    <input type="text" class="effect-7" id="time-1" name="time[1]" required><br>
                                                    <span class="focus-border">
                                                        <i></i>
                                                    </span>
                                                 </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="col-3">
                                                    <input placeholder="@lang('manageSchedule.placeholder.room')" type="text" class="effect-7" id="room-1" name="room[1]" required><br>
                                                    <span class="focus-border"><i></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                             <div class="col-md-3">
                                                <input type="checkbox" id="day-2" name="day[2]" value="2" checked>@lang('manageSchedule.days.tue')
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-3">
                                                    <input type="text"  class="effect-7" id="time-2" name="time[2]" required>
                                                    <span class="focus-border">
                                                        <i></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="col-3">
                                                    <input placeholder="@lang('manageSchedule.placeholder.room')" type="text" class="effect-7" id="room-2" name="room[2]" required><br>
                                                    <span class="focus-border"><i></i></span>
                                                </div>
                                            </div>
                                        </div>
                                       <br>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <input type="checkbox" id="day-3" name="day[3]" value="3" checked>@lang('manageSchedule.days.wed')
                                            </div>
                                            <div class="col-md-6" style="float: left;">
                                                <div class="col-3" >
                                                 <input type="text" class="effect-7" id="time-3" name="time[3]" required>
                                                 <span class="focus-border">
                                                        <i></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="col-3">
                                                    <input placeholder="@lang('manageSchedule.placeholder.room')" type="text" class="effect-7" id="room-3" name="room[3]" required><br>
                                                    <span class="focus-border"><i></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <input type="checkbox" id="day-4" name="day[4]" value="4" checked>@lang('manageSchedule.days.thu')
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-3">
                                                 <input type="text" class="effect-7" id="time-4" name="time[4]" required>
                                                 <span class="focus-border">
                                                        <i></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="col-3">
                                                    <input placeholder="@lang('manageSchedule.placeholder.room')" type="text" class="effect-7" id="room-4" name="room[4]" required><br>
                                                    <span class="focus-border"><i></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-3">
                                                 <input type="checkbox" id="day-5" name="day[5]" value="5" checked>@lang('manageSchedule.days.fri')
                                            </div>
                                            <div class="col-md-6">
                                                 <div class="col-3">
                                                 <input type="text" class="effect-7" id="time-5" name="time[5]" required>
                                                 <span class="focus-border">
                                                        <i></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="col-3">
                                                    <input placeholder="@lang('manageSchedule.placeholder.room')" type="text" class="effect-7" id="room-5" name="room[5]" required><br>
                                                    <span class="focus-border"><i></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <input type="checkbox" id="day-6" name="day[6]" value="6" checked>@lang('manageSchedule.days.sat')
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-3">
                                                 <input type="text" id="time-6" class="effect-7" name="time[6]" required>
                                                 <span class="focus-border">
                                                        <i></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="col-3">
                                                    <input placeholder="@lang('manageSchedule.placeholder.room')" type="text" class="effect-7" id="room-6" name="room[6]" required><br>
                                                    <span class="focus-border"><i></i></span>
                                                </div>
                                            </div>
                                        </div>                       
                                    </div>
                                @endif
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div class="form-group" style="padding: 10px;">
                        <div class="row">
                            <div class="box-header" style="margin-bottom:-20px!important;">
                              <h3 class="box-title">@lang('manageSchedule.input.sduration')</h3>
                              <div class="box-tools pull-right">
                                <button type="button" class="btn bg-teal btn-sm" data-toggle="collapse" data-parent="#accordion" href="#slotDuration" aria-expanded="true" aria-controls="collapseOne"><i class="fa fa-caret-down"></i>
                                </button>
                              </div>
                            </div>
                            <hr style="border-color: #4FC3A1;">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div id="slotDuration" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingOne">
                                  <div class="col-md-12">
                                    @if(isset($setting))
                                    <div class="col-3">
                                    <select name="slotDuration" style="width: 100%;" class="effect-7">
                                        <option value="00:05">5 @lang('manageSchedule.times.minutes')</option>
                                        <option value="00:10">10 @lang('manageSchedule.times.minutes')</option>
                                        <option value="00:15">15 @lang('manageSchedule.times.minutes')</option>
                                        <option value="00:20">20 @lang('manageSchedule.times.minutes')</option>
                                        <option value="00:25">25 @lang('manageSchedule.times.minutes')</option>
                                        <option value="00:30">30 @lang('manageSchedule.times.minutes')</option>
                                        <option value="00:35">35 @lang('manageSchedule.times.minutes')</option>
                                        <option value="00:40">40 @lang('manageSchedule.times.minutes')</option>
                                        <option value="00:45">45 @lang('manageSchedule.times.minutes')</option>
                                        <option value="00:50">50 @lang('manageSchedule.times.minutes')</option>
                                        <option value="00:55">55 @lang('manageSchedule.times.minutes')</option>
                                        <option value="01:00">1 @lang('manageSchedule.times.hour')</option>
                                    </select>
                                    <span class="focus-border"> <i></i></span>
                                    </div>
                                    @endif
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" style="padding: 10px;">
                        <div class="row">
                            <div class="box-header" style="margin-bottom:-20px!important;">
                              <h3 class="box-title">@lang('manageSchedule.input.themes')</h3>
                              <div class="box-tools pull-right">
                                <button type="button" class="btn bg-teal btn-sm" data-toggle="collapse" data-parent="#accordion" href="#themesCalendar" aria-expanded="true" aria-controls="collapseOne"><i class="fa fa-caret-down"></i>
                                </button>
                              </div>
                            </div>
                            <hr style="border-color: #4FC3A1;">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div id="themesCalendar" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingOne">
                                  <div class="col-md-12">
                                    @if(isset($setting))
                                    <div class="col-3">
                                    <select name="themesCalendar" style="width: 100%;" class="effect-7">
                                        <option value="classic">Classic</option>
                                        <option value="flat">Flat</option>
                                    </select>
                                    <span class="focus-border"> <i></i></span>
                                    </div>
                                    @endif
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <center>
                             <button type="submit" class="btn btn-block btn-success">@lang('manageSchedule.input.save')</button>
                             @if(isset($report))
                                <p style="color:red; font-size: 13px"><b>{{$report}}</b>
                             @endif
                        </center>
                    </div>
                </form>
            </div>      
        </div>
            <div class="col-md-7" id="box-calendar">
                <div class="box-header " >
                    <br>
                  <div class="box-tools pull-right">
                    <h3 class="box-title">@lang('manageSchedule.cal_bar')</h3>
                    <button type="button" class="btn bg-teal btn-sm" data-toggle="collapse" data-parent="#accordion"  id="minCalendar" aria-expanded="true" aria-controls="collapseOne"><i class="fa fa-minus"></i>
                    </button>
                  </div>
                </div>
                @if(isset($setting))
                <div class="jumbotron viewcal" id="panelCalendar">
                <div class="panel panel-default">
                    <div class="panel-body" id="calendarForm">
                        {!! $calendar->calendar() !!}
                    </div>
                </div>
            </div>
            @endif
        </div>
        
    </div>
    <form action="/schedule/manage/create" method="POST" accept-charset="utf-8">
        {{csrf_field()}}
         <div class="modal fade" id="create-schedule">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Create New Scehdule</h4>
                    </div>
                    <form>
                        <div class="modal-body">
                            <div class="panel panel-default">
                                <div class='input-group date'>
                                    <input type='text' placeholder="Names of Schedule. Ex : Hari Tenang" id="uname_sch" class="form-control" onchange="check()" name="newSchedule" />
                                    <span class="input-group-addon" style=" padding:0!important;">
                                        <button type="submit" class="btn btn-primary">Apply</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </form>
   

    {!! $calendar->script() !!}
    
<script>
    $(document).ready(function(){
        showTime();
        var idPreset = $("input[name='idPreset']").val();
        var hiddenDays = myval.setting['hiddenDays'];
        var businessHours = myval.setting['businessHours'];
        var slotDuration = myval.setting['slotDuration'];
        var theme = myval.theme;
        var preset = myval.preset;
        var userPreset = myval.userPreset;
        var place = myval.preset[idPreset]['rooms'];
        $('#presetSelected').html(userPreset);
        $(".fc-next-button, .fc-prev-button, .fc-today-button").on("click",function(){
            showTime();
        })
        $("#minCalendar").on("click",function(e){
            e.preventDefault();
            $("#panelCalendar").fadeToggle('2000',function(){
                if($(this).css('display') == 'none'){
                    $("#panelSetting").attr('class','col-md-10'); 
                    $("#box-calendar").attr('class','col-md-2').css("top","250px");
                }else{
                    $("#panelSetting").attr('class','col-md-5');
                    $("#box-calendar").css("top","0px").attr('class','col-md-7');
                }
                
            });
            
        })

        $('select option[value="' + slotDuration + '"]').attr("selected",true);
        $('select option[value="' + theme + '"]').attr("selected",true);
        $.each(preset,function(index,value){
            $("#preset-data").prepend("<li><a href='#' data-preset='"+value['id']+"' class='list-preset'>"+value['name']+"</a></li>");
        })

        $.each(hiddenDays,function(index,value){
            $("#day-"+value).removeAttr("checked");
            $("#room-"+value).removeAttr("required").prop("disabled",true);
            $("#time-"+value).removeAttr("required").prop("disabled",true);
        })
        $.each(businessHours,function(index,value){
           var days = businessHours[index]['dow'].split(",");
            for (var countdays = -1; countdays < days.length; countdays++) {
                for (var i = -1 ; i<7; i++) {
                    if(days[countdays] == i){
                        if($("#time-"+i).val() != ""){
                            var getTime = $("#time-"+i).val();
                            console.log(getTime);
                            $("#time-"+i).val(getTime + ";" +businessHours[index]['start']+"-"+businessHours[index]['end']);
                            $("#room-"+i).val(place[i]);

                        }else{
                            $("#time-"+i).val(businessHours[index]['start']+"-"+businessHours[index]['end']);
                            $("#room-"+i).val(place[i]);
                        }
                        
                    }else{
                        continue;
                    }
                }
            }
        });

        $(".list-preset").on("click",function(e){
            e.preventDefault();
            var name = $(this).html();
            $('#presetSelected').html(name);
            $('#editPreset').val(name)
            //alert($(this).attr('data-preset'));
            presetLoad($(this).attr('data-preset'));
        })

        $("input[type='checkbox']").on("click",function(){
           var idCheckbox = $(this).attr('id').replace('day-','');
           if($(this).prop('checked') == false){
             $("#room-"+idCheckbox).removeAttr('required').prop('disabled',true);
             $("#time-"+idCheckbox).removeAttr('required').prop('disabled',true);
           }else{
             $("#room-"+idCheckbox).removeAttr('disabled').prop('required',true);
             $("#time-"+idCheckbox).removeAttr('disabled').prop('required',true);
           }
        })
    });


    function showTime(){
        var timeLoad = $('tr[data-time]').map( function() {
            return $(this).attr('data-time');
        }).get();
        $.each(timeLoad,function(index,value){
            $(".fc-content-col").append("<div class='button-grid'><p style='margin-bottom:0px;'>"+value+"</p></div>");
        })
    }  
    function presetLoad(id){
        console.log(myval.preset[id]);
        //alert(myval.setting[1]['hiddenDays']);
        var hiddenDays = myval.preset[id]['hiddenDays'];
        var minTime = myval.preset[id]['minTime'];
        var maxTime = myval.preset[id]['maxTime'];
        var businessHours = myval.preset[id]['businessHours'];
        var slotDuration = myval.preset[id]['slotDuration'];
        var theme = myval.preset[id]['themes'];
        var place = myval.preset[id]['rooms'];

        $('input[name="idPreset"]').val(id);
        $('#minTime').val(function(index,value){
            return value.replace(value,minTime);
        })
        $('#maxTime').val(function(index,value){
            return value.replace(value,maxTime);
        })
        $('select[name="slotDuration"]').val(slotDuration);
        $('select[name="themesCalendar"]').val(theme);
        
        for(x=0;x<=6;x++){
            $("#day-"+x).prop("checked",true);
        }
        for(x=0;x<=6;x++){
            $("#time-"+x).val("");
            $("#room-"+x).val("");
        }
        $.each(hiddenDays,function(index,value){
            $("#day-"+value).removeAttr("checked");
            $("#room-"+value).removeAttr("required").prop("disabled",true);
            $("#time-"+value).removeAttr("required").prop("disabled",true);
        })

        $.each(businessHours,function(index,value){
           var days = businessHours[index]['dow'].split(",");
            for (var countdays = -1; countdays < days.length; countdays++) {
                for (var i = 0 ; i<7; i++) {
                    if(days[countdays] == i){
                        if($("#time-"+i).val() != ""){
                            var getTime = $("#time-"+i).val();
                            $("#time-"+i).val(getTime + ";" +businessHours[index]['start']+"-"+businessHours[index]['end']);
                            $("#room-"+i).val(place[i]);
                        }else{
                            $("#time-"+i).val(businessHours[index]['start']+"-"+businessHours[index]['end']);
                            $("#room-"+i).val(place[i]);
                        }
                        
                    }else{
                        continue;
                    }
                }
            }
        });
        for(x=0;x<=6;x++){
            if($("#day-"+x).prop("checked") == true){
                $("#room-"+x).removeAttr('disabled').prop('required',true);
                $("#time-"+x).removeAttr('disabled').prop('required',true);
            }
        }
    }  

// $(".fc-widget-content").html(function(){
//     var time = $(this).parent().attr('data-time');
//     return time;
    
// })
</script>
@endsection
