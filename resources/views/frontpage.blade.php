@extends('layouts.base')
@section('content')
@include('varviewjs')
<!-- Main component for call to action -->
<style>
body {
    background-color: #cae6e3!important;
}
.jumbotron {
    background-color: #4f929c!important;
}
.info-box {
    background: #1e494e!important;
    color: white!important;
    box-shadow: 0 1px 5px #271f1f!important;

}
</style>
<div class="jumbotron">
    <div class="row">
        <div class="col-md-4">
           <img src="{{$detail['photo']}}" alt="Email Photo" width="300px">
        </div>
        <div class="col-md-8">
            <h4><i>  @lang('frontpage.as_status') {{Auth::user()->user_unique}} </i></h4>
            @if($user_unique == "Dosen")
            <h2>
                @lang('frontpage.hello_greeting'), {{Auth::user()->name}}
            </h2>
            <h1>
                 @lang('frontpage.title_dashboard')
            </h1>
            <p>
                @lang('frontpage.desc_dashboard')
            </p>
            @else
             <h2>
                @lang('frontpage.hello_greeting'), {{Auth::user()->name}}
            </h2>
            <h1>
                Make Your Appointment
            </h1>
            <p>
                The easiest way to you contact and make an appointment to your lecturer with <b>Integration of Google Calendar </b> System Information.
            </p>
            @endif
        </div>
    </div>
    
   
</div>
<div class="row">
  @if($user_unique == "Dosen")
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="ion ion-ios-people-outline"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">@lang('frontpage.total_app')</span>
          <span class="info-box-number">{{$detail['totalApp']}}<small> App</small></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="ion ion-checkmark-circled"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">@lang('frontpage.active_app')</span>
          <span class="info-box-number">{{$detail['totalActiveApp']}}<small> App</small></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-orange"><i class="ion ion-android-watch"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">@lang('frontpage.last_manage')</span>
          <span class="info-box-number">{{$detail['lastUpdate']}}<small></small></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
  @else
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="ion ion-ios-people-outline"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Total Appointment</span>
          <span class="info-box-number">{{$detail['totalApp']}}<small> App</small></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="ion ion-checkmark-circled"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Latest Appointment</span>
          <span class="info-box-number">At {{$detail['latestApp']}}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-red"><i class="ion ion-close-circled"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Reschedule/Cancel App</span>
          <span class="info-box-number">{{$detail['totalNonActiveApp']}}<small> App</small></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
  @endif
</div>
<div class="modal fade" id="Introduction">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Introduction for the First Time</h4>
                    </div>
                    <form>
                        <div class="modal-body">
                                Please follow this step, so that your account can be used <br>
                                1. Go To <b>Setting</b> <br>
                                2. Check gCalendar Column <br>
                                3. Integrate your account within <b>Calendar Google Account</b>. <br>
                                4. After that, Go To <b>Schedule</b><br>
                                5. and klik <b>Create Schedule</b> -> <b>Create New</b>. <br>
                                6. Done, you can manage now.<br>
                            <div class="modal-footer">
                                <!-- <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button> -->
                               
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
<script>
  if(myval.isFirst == 1){
     $('#Introduction').modal('show');
  }
</script>

<!-- /container -->
@endsection
