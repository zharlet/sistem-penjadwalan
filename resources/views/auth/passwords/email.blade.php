@extends('layouts.app')

<!-- Main Content -->
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="#">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Forgot Password ?</label>

                            <div class="col-md-6" id="resetPass">
                                <a href="#"  class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Reset with your Google Account</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
     $("#resetPass").on("click",function(e){
      e.preventDefault();
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      $.ajax({
        url: '/reset/password',
        type: 'POST',
        data: {
            _token: CSRF_TOKEN,
        },
        dataType: 'JSON',
        success: function (data) { 
     
            window.location.href = "/auth/redirect/google";
        }
      });
    });
</script>
@endsection
