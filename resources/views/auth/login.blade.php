@extends('layouts.app')
@section('content')
@include('varviewjs')
<body class="hold-transition login-page">
    <div class="login-box">
  <div class="login-logo">
    <a href="/"><b>Scheduler &nbsp;</b>System</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    <form method="post" role="form" action="{{ url('/login') }}" >
      {{ csrf_field() }}
      <div class="form-group has-feedback">
        <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Email" required autofocus>
        @if ($errors->has('email'))
              <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
              </span>
        @endif
      </div>
      <div class="form-group has-feedback">
         <input id="password" placeholder="Password" type="password" class="form-control" name="password" required>
          @if ($errors->has('password'))
              <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
              </span>
          @endif
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <div class="social-auth-links text-center">
      <p>- Not Registered ? -</p>
      <a href="{{ url('/auth/redirect/google') }}" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Register using
        Google+</a>
    </div>
    <!-- /.social-auth-links -->

    <a href="/password/reset">I forgot my password</a><br>
  </div>
  <!-- /.login-box-body -->
</div>
</div>
</body>
<script>
  console.log(myval.cobbaa);
</script>
@endsection
