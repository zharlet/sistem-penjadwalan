@extends('layouts.base')
@section('content')
@include('varviewjs')
    <script>
        $(document).ready(function(){
            $("nav").hide();
            var at = null;
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: '/ajax/getAccessToken/',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN
                },
                dataType: 'JSON',
                success: function (data) { 
                    console.log(data);
                }
            });          
       });    
    </script>
    <style>
        tr{
            height: 30px;
        }
    </style>
    <br>    
    <form action="{{route('createapp.store')}}" method="POST" role="form">
        {{csrf_field()}}
        <legend>
            Create Appointment to {{$name}}
        </legend>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="title">
                        Name
                    </label>
                    <input class="form-control" name="title" placeholder="Name" value="{{ $myname }}" type="text" readonly>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="description">
                        Description
                    </label>
                    <br>
                    <select class="form-control" name="description">
                        <option value="Bimbingan">Bimbingan</option>
                        <option value="Konsultasi">Konsultasi</option>
                        <option value="Presentasi">Presentasi</option>
                    </select>
                    <textarea class="form-control" name="information" placeholder="Additional Information" type="text" rows="4" cols="50" required></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="place">
                        Place
                    </label>
                    <input class="form-control" name="place" placeholder="Place" type="text" rows="4" cols="50" readonly>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label for="description">
                    Choose Date & Time
                </label>              
                <div class="panel panel-default">
                    <div class="panel-body" id="calendarForm">
                        {!! $calendar->calendar() !!}
                    </div>
                    
                </div>
                <div>
                @if($error != "")
                    <p style="color:red;"><b>{{ $error }}</b></p>
                @endif
                </div>
            </div>
        </div>
<!--         <input type="hidden" name="atoken" id="atoken"> -->
        <!-- <input type="hidden" name="expires_in" value="3600"> -->
        <!-- <input type="hidden" name="rtoken" value={{$refresh_token}}> -->
       <!--  <input type="hidden" name="scope" value="https://www.googleapis.com/auth/calendar"> -->
        <div class="form-group">
            <input class="form-control" name="start_date" type="hidden" id="start_date" disabled>
        </div>
        <button class="btn btn-primary" type="submit" id="submit">
            Submit
        </button>
        <br>    
        <br>    

    </form>
    {!! $calendar->script() !!}
    
<script>
    $(document).ready(function(){
        
        showTime();
        $(".fc-next-button, .fc-prev-button, .fc-today-button").on("click",function(){
            showTime();
        })
        $(".fc-view").on('dragstart',function(e){
            e.preventDefault();
        }).attr('draggable','false');

    })

    function showTime(){
        var timeLoad = $('tr[data-time]').map( function() {
            return $(this).attr('data-time');
        }).get();
        $.each(timeLoad,function(index,value){
            $(".fc-content-col").append("<div class='button-grid'><p style='margin-bottom:0px;'>"+value+"</p></div>");
        })
    }
    console.log("{{route('createapp.store')}}");
    // $("#submit").on("click", function(){
    //     swal({
    //         title: "Success",
    //         text: "Your appointment success input",
    //         icon: "success",
    //         button: "Close!",
    //     });
    // })
</script>
@endsection
