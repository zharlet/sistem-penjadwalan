<!DOCTYPE html>
<html>
<head>

    <!-- load jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- provide the csrf token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <script>
        $(document).ready(function(){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var ci = $('#client_id').val();
            var cs = $('#client_secret').val();
            var rt = $('#refresh_token').val();
            var gt = $('#grant_type').val();
            $(".postbutton").click(function(){
                $.ajax({
                    /* the route pointing to the post function */
                    url: 'https://www.googleapis.com/oauth2/v4/token/',
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: {
                        _token: CSRF_TOKEN, 
                        client_id: ci ,
                        client_secret: cs ,
                        refresh_token: rt,
                        grant_type: gt

                    },
                    dataType: 'JSON',
                    /* remind that 'data' is the response of the AjaxController */
                    success: function (data) { 
                        console.log(data['access_token']);
                    }
                }); 
            });
       });    
    </script>

</head>

<body>
    <button class="postbutton">Post via ajax!</button>
    <input type="hidden" name="client_id" value="276645278122-njmsnjj6m57849t4me70hd1lcmps7lj0.apps.googleusercontent.com" id="client_id">
    <input type="hidden" name="client_secret" value="WXiMQQfV0DHX02jlivUggTVp" id="client_secret">
    <input type="hidden" name="refresh_token" value="1/cQYoOTn-kHOaoWkw384uIePrn3MEfvAqZCA_q7eLMlU" id="refresh_token">
    <input type="hidden" name="grant_type" value="refresh_token" id="grant_type">
<div class="writeinfo"></div>   
</body>

</html>