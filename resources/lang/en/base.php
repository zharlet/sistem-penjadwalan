<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'setting_menu' => 'Setting',
    'check_menu' => 'Check App',
    'sch_menu' => 'Schedule',
    'logout_menu' => 'Logout',
    'as_status' => 'As',
    'hello_greeting' => 'Hello',
    'title_dashboard' => 'Manage Your Appointment',
    'desc_dashboard' => 'The easiest way to you to manage all yours appointment with Integration of Google Calendar System Information.',
    'total_app' => 'Total Appointment',
    'active_app' => 'Active Appointment',
    'last_manage' => 'Last Manage Schedule',

];
