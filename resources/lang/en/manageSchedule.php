<?php

return [
    'title' => 'Manage Schedule',
    'cal_bar' => 'View Calendar',
    'input' => [
        'preset' => 'Presets',
        'stime' => 'Show Time',
        'dshow' => 'Days Show & Rooms',
        'sduration' => 'Slot Duration',
        'themes' => 'Calendar Themes',
        'create' => 'New Schedule',
        'save' => 'Save',
    ],
    'days' => [
        'mon' => 'Monday',
        'tue' => 'Tuesday',
        'wed' => 'Wed',
        'thu' => 'Thursday',
        'fri' => 'Firday',
        'sat' => 'Saturday',
        'sun' => 'Sunday',
    ],
     'times' => [
        'hour' => 'Hour',
        'minutes' => 'Minutes'
    ],
    'placeholder' => [
        'room' => 'room'
    ]
    
    
];