<?php

return [
    'title' => 'Appointment Table',
    'table' => [
    	'name' => 'Name',
    	'date' => 'Date',
    	'time' => 'Time',
    	'place' => 'Place',
    	'action' => 'Action',
    	'save' => 'Save',
    ],
    
];