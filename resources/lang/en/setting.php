<?php

return [
    'title' => 'Setting Your Account',
    'description' => 'Your setting is needed',
    'input' => [
    	'ua' => 'Username Access',
    	'name' => 'Name',
    	'password' => 'Password',
    	'gcal_connected' => 'Your account has been connected with Google Calendar',
    	'gcal_null' => 'Your account is not connected with Google Calendar',
    	'save' => 'Save',
    ],
    
];