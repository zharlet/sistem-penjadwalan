<?php

return [
    'as_status' => 'As',
    'hello_greeting' => 'Hello',
    'title_dashboard' => 'Manage Your Appointment',
    'desc_dashboard' => 'The easiest way to you to manage all yours appointment with Integration of Google Calendar System Information.',
    'total_app' => 'Total Appointment',
    'active_app' => 'Active Appointment',
    'last_manage' => 'Last Manage Schedule',
];