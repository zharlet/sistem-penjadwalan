<?php

return [
 	'as_status' => 'Sebagai',
    'hello_greeting' => 'Hallo',
    'title_dashboard' => 'Atur Perjanjian Anda',
    'desc_dashboard' => 'Cara mudah untuk mengatur semua perjanjian anda yang terhubung dengan Sistem Informasi Google Calendar',
    'total_app' => 'Jumlah Perjanjian',
    'active_app' => 'perjanjian Aktif',
    'last_manage' => 'Perubahan Terakhir Jadwal',
];