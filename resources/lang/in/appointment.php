<?php

return [
    'title' => 'Tabel Perjanjian',
    'table' => [
    	'name' => 'Nama',
    	'date' => 'Tanggal',
    	'time' => 'Waktu',
    	'place' => 'Tempat',
    	'action' => 'Action',
    	'save' => 'Simpan',
    ],
    
];