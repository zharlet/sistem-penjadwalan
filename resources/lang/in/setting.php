<?php

return [
    'title' => 'Atur Akun Anda',
    'description' => 'Pengaturan anda dibutuhkan',
    'input' => [
    	'ua' => 'Akses Username',
    	'name' => 'Nama',
    	'password' => 'Kata Sandi',
    	'gcal_connected' => 'Akun anda telah terhubung dengan Google Calendar',
    	'gcal_null' => 'Akun anda belum terhubung dengan Google Calendar',
    	'save' => 'Simpan',
    ],
    
];