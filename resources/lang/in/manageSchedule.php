<?php

return [
    'title' => 'Atur Jadwal',
    'cal_bar' => 'Tampilan Kalendar',
    'input' => [
    	'preset' => 'Setelan',
    	'stime' => 'Tampilan Waktu',
    	'dshow' => 'Tampilan Hari & Ruangan',
    	'sduration' => 'Durasi Perjanjian',
    	'themes' => 'Tema Kalendar',
        'create' => 'Buat Jadwal',
    	'save' => 'Simpan',
    ],
    'days' => [
        'mon' => 'Senin',
        'tue' => 'Selasa',
        'wed' => 'Rabu',
        'thu' => 'Kamis',
        'fri' => 'Jumat',
        'sat' => 'Sabtu',
        'sun' => 'Minggu',
    ],
    'times' => [
        'hour' => 'Jam',
        'minutes' => 'Menit'
    ],
    'placeholder' => [
        'room' => 'ruang'
    ]
    
];