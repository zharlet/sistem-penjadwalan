<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'setting_menu' => 'Pengaturan',
    'check_menu' => 'Cek Perjanjian',
    'sch_menu' => 'Jadwal',
    'logout_menu' => 'Keluar',
    'as_status' => 'Sebagai',
    'hello_greeting' => 'Hallo',
    'title_dashboard' => 'Atur Perjanjian Anda',
    'desc_dashboard' => 'Cara mudah untuk mengatur semua perjanjian anda yang terhubung dengan Sistem Informasi Google Calendar',
    'total_app' => 'Jumlah Perjanjian',
    'active_app' => 'perjanjian Aktif',
    'last_manage' => 'Perubahan Terakhir Jadwal',
];
