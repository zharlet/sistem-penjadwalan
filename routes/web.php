<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
 */

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth');

Route::get('/ajax/getAccessToken/', 'ajax\getAccessToken@index');
Route::post('/ajax/getAccessToken/', 'ajax\getAccessToken@index');
Route::get('oauth', 'CreateAppointment@oauth')->name('oauthCallback');
Route::get('/home','HomeController@index');
Route::get('/coba','CheckAppointment@lol');
Route::post('/reset/password',function(){
	\Session::put('action_password_reset','reset');
	return response()->json('submit');
});
Route::post('/reset/password/confirm','Auth\ResetPasswordController@index');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('createapp', 'CreateAppointment');
    Route::get('/', 'HomeController@index');
    Route::get('/setting','AccountSetting@index');
    Route::get('/logout','Auth\LogoutController@getLogout');
    Route::post('/password/change','AccountSetting@passwordChange');
    Route::post('/setting/save','AccountSetting@update');
    Route::post('/setting/email',function(){
		\Session::put('action_mail_update','update');
		return response()->json('submit');
	});
	Route::get('locale/{locale}', function($locale) {
	    Session::put('locale',$locale);
	    return redirect()->back();
	});
});

Route::group(['middleware' => 'student'], function() {
    Route::get('/appointment/create/{username}', 'CreateAppointment@create');
    Route::get('/make/appointment/', 'ajax\setAppointment@index');
	Route::post('/make/appointment/', 'ajax\setAppointment@index');
	Route::post('/appointment/store/','CreateAppointment@store');
	Route::get('/appointment/store/','CreateAppointment@store');
	Route::get('/appointment/list','CheckAppointment@checkAppStudent');
	Route::get('/search/lecturer','ajax\searchLecturer@index');
	Route::post('/reschedule/deny/callback','CheckAppointment@setDenyCallback');
	Route::post('/reschedule/accept/callback','CheckAppointment@setAcceptCallback');
});

Route::group(['middleware' => 'lecturer'],function(){
	
	
	Route::get('/schedule/manage','LecturerSchedule@index');
	Route::get('/appointment/check','CheckAppointment@checkAppLecturer');
	Route::post('/schedule/manage/save', 'LecturerSchedule@update');
	Route::post('/schedule/manage/create', 'LecturerSchedule@create');
	Route::post('/appointment/reschedule/callback','CheckAppointment@setRescheduleCallback');
	Route::post('/appointment/cancel/callback','CheckAppointment@setCancelCallback');
	
});

Route::get('ajax', function(){ return view('refreshToken'); });
Route::post('https://www.googleapis.com/oauth2/v4/token/','RefreshTokenController@post');

Auth::routes();
Route::resource('createapp', 'CreateAppointment');
Route::get('oauth', ['as' => 'oauthCallback', 'uses' => 'CreateAppointment@oauth']);

// Google Login
Route::get('/auth/redirect/google', 'SocialController@redirectToProvider');
Route::get('/callback/google/', 'SocialController@handleProviderCallback');

