<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_Preset extends Model
{
    public $table = "presets";
    protected $fillable = ['id','name', 'id_users','hiddenDays','minTime','maxTime','slotDuration','businessHours','rooms','themes'];
}
