<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class M_ManageSchedule extends Model
{
	public $table = 'manage_schedules';
    protected $fillable = ['id_users','id_presets'];
}
