<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_TokenUser extends Model
{
    public $table = "token_users";
    public $timestamps = false;
}
