<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RefreshTokenController extends Controller
{
    public function post(Request $request){
      $response = array(
          'status' => 'success',
          'msg' => $request,
      );
      return response()->json($response); 
   }
}
