<?php

namespace App\Http\Controllers;
use Auth;
use Session;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccountSetting extends Controller
{
	public function __construct(){
		$this->middleware('auth');
	}
    public function index(){
        $report = "";
        if(Session::get('report') != null){
            $report = Session::get('report');
        }
    	$bool = true;
    	$user_id = $this->check();

        $read_setting = $this->readSetting();
        if(Auth::user()->user_unique == "Dosen"){
            if($user_id->refresh_token == "" || $user_id->refresh_token == null  ){
                $bool = "true";
            }else{
                $bool = "false";
            }
        }else{
            $bool = "true";
        }
    	
        
    	return view('settingaccount',['bool' => $bool , 'setting' => $read_setting, 'report' => $report]);
    }
    public function check(){
        if(Auth::user()->user_unique == 'Mahasiswa'){
            return null;
        }
    	$user_id = Auth::user()->id;
    	$refresh_token = DB::table('token_users')->where('id_users', '=', $user_id)->first();
    	return $refresh_token;
    }
    public function readSetting(){
        if(Auth::user()->user_unique == 'Mahasiswa'){
            return User::select('name','email')->where('id', Auth::id())->first();
        }
        return User::select('name','email','gcalendar','username_sch','isOnline')->where('id', Auth::id())->first();

    }
    public function update(Request $req){
        if(Auth::user()->user_unique == 'Mahasiswa'){
            $result = User::where('id',Auth::id())->update([
                'name' => $req->name
            ]);
        }else{
            $isOnline = isset($req->isOnline) ? 1 : 0;
            try {
                $result = User::where('id',Auth::id())->update([
                    'name' => $req->name,
                    'username_sch' => $req->uname_access,
                    'isOnline' => $isOnline
                ]);
            } catch (\Illuminate\Database\QueryException $e) {
                $errorCode = $e->errorInfo[1];
                if($errorCode == 1062){
                    return back()->with('report','Username has been use by other');
                }
            }
        }
        if(!$result){
            return back()->with('report','Setting Not Saved') ;
        }
        return back()->with('report','Setting Saved') ;
        
    }
    public function passwordChange(Request $req){
        $password = $req->password;
        $password_confirmed = $req->password_confirmation;
        if(strcmp($password, $password_confirmed) != 0){
            return redirect()->back()->with('report',"Password Not Match");
        }
        $validate = $req->validate([
            'password' => 'required|string|min:6|confirmed'
        ]);
        User::where('id',Auth::id())->update([
            'password' => bcrypt($password)
        ]);
        return redirect()->back()->with('report',1);
    }
}
