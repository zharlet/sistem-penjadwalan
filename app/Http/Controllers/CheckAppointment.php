<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Redirect;
use JavaScript;
use App\M_Appointment;
use App\M_TokenUser;
use App\M_Preset;
use App\User;
use App\M_ManageSchedule;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;

use Carbon\Carbon;
use Google_Client;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;
use Google_Service_Calendar_EventDateTime;

class CheckAppointment extends CreateAppointment
{
    // public function __construct(){
    // 	$this->middleware('auth');
    // }

    // public function index(){

    // }
	public function checkAppLecturer(){
		$pageSpec = false;
		if (\Request::is('appointment/check'))
		{
		    $pageSpec = true;
		}
		$events = [];
		$eventformat = [];
		$btformat = "";
    	$setting = null;
    	$businessHours = [];
        $userPreset = M_ManageSchedule::select('id_presets')->where('id_users',Auth::id())->first();
    	$data = M_Preset::select('hiddenDays','minTime','maxTime','slotDuration','businessHours')->where('id',$userPreset['id_presets'])->get()->toArray();
    	$y = 0;
    	foreach ($data as $key) {
    		$getKeyBH = $key['businessHours'];
    		$getKeyHD = $key['hiddenDays'];
    		$spliting = preg_split('/[;-]/', $getKeyBH, null, PREG_SPLIT_NO_EMPTY);
    		$splitcomma = array_map('intval', preg_split('/[,]/', $getKeyHD, null, PREG_SPLIT_NO_EMPTY));
    		for($x=0; $x < count($spliting)/5; $x++){
            $temp = [
                'overlap' => $spliting[$y],
                'dow' => $spliting[$y+1],
                'start' => $spliting[$y+2],
                'end' => $spliting[$y+3],
            ];
	            array_push($businessHours, $temp);
	            $y+=5;
	        }
	        $key['businessHours'] = $businessHours;
	        $key['hiddenDays'] = $splitcomma;
	        $key['height'] = 700;
	        $setting = $key;

    	}
    	
    	$appointment = M_Appointment::where('to_user',Auth::id())->get();

        if($appointment->count()) {
            foreach ($appointment as $key => $value) {
                
                $events[] = Calendar::event(
                    $value->description . " (" . $value->name . ")",
                    false,
                    $value->start_date,
                    $value->end_date,
                    $value->id,
                    // Add color and link on event
                 [
                    'color' => $value->color,
                 ]
                );
            }
            foreach ($appointment as $key => $value){
                $splitDateTime = explode("T", $value->start_date);
            	$splitDateTimeEnd = explode("T", $value->end_date);
                
            	$replaceDate = str_replace("-", "/", $splitDateTime[0]);
            	$dateformat = date("d/m/Y" , strtotime($replaceDate));

            	$timeformat = $splitDateTime[1];
                $strings = preg_split ('/:/', $timeformat, 3);
                $second=array_pop($strings);
                $first=implode(":", $strings);
                $start_time = $first;
                
                $timeformat = $splitDateTimeEnd[1];
                $strings = preg_split ('/:/', $timeformat, 3);
                $second=array_pop($strings);
                $first=implode(":", $strings);
                $end_time = $first;

            	$eventformat[] = [
            		"Name" => $value->name,
            		"Date" => $dateformat,
            		"Time" => $start_time . " - " . $end_time,
            		"Status" => $value->status,
                    "ID" => $value->id,
            		"Place" => $value->place
            	];
            }
           
        };
        // $get_RToken = M_TokenUser::select('refresh_token')->where('id_users',Auth::id())->first();
        // \Session::put('RToken',$get_RToken['refresh_token']);
        $calendar = $this->fullCalendarExtends($events, $setting);
        Javascript::put([
            'lang' => \Session::get('locale')
        ]);
        
    	return view('appointmentPage',[ 'calendar' => $calendar , 'pageSpec' => $pageSpec , 'appointment' => $eventformat]);
	}

	public function checkAppStudent(){
		$eventformat = [];
		$splitRDateTime = "";
		$replaceRDate = "";
		$rdateformat = "";
		$rtimeformat = "";
		$appointment = M_Appointment::where('id_user',Auth::id())->get();
		foreach ($appointment as $key => $value){
            $lecturer = User::where('id',$value->to_user)->first();
        	$splitDateTime = explode("T", $value->start_date);
        	if($value->reschedule_date != null){
        		$splitRDateTime = explode("T", $value->reschedule_date);
        		$replaceRDate = str_replace("-", "/", $splitRDateTime[0]);
        		$rdateformat = date("d/m/Y" , strtotime($replaceRDate));
        		$rtimeformat = $splitRDateTime[1];
        		$rdateformat = date("d/m/Y" , strtotime($replaceRDate));
        	}
        	$replaceDate = str_replace("-", "/", $splitDateTime[0]);
        	$dateformat = date("d/m/Y" , strtotime($replaceDate));
        	
        	$timeformat = $splitDateTime[1];
        	$status = $value->status == "Waiting" ? "Reschedule to " . $rdateformat . " " . substr($rtimeformat, 0, strpos($rtimeformat, "+")) : $value->status;
        	$eventformat[] = [
        		"Name" => $lecturer['name'],
        		"Date" => $dateformat,
                "Time" => $timeformat,
        		"Place" => $value->place,
        		"Status" => $status,
        		"OriStatus" => $value->status,
        		"ID" => $value->id
        	];
        }
        return view('eventPage',['events' => $eventformat ]);
	}
	public function fullCalendarExtends($events, $setting){
        $setOption = [];
        $calendar = Calendar::addEvents($events)->setOptions($setting)->setCallbacks([
        'dayClick' => "
        function(date, jsEvent) {},
        select: function(startDate, endDate, jsEvent) {
          var startDateStr = startDate.format();
          var endDateStr = endDate.format();
          var CSRF_TOKEN = $('meta[name=\"\csrf-token\"\]').attr('content');
          $.ajax({
                url: '/make/appointment/',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN, 
                    startDate: startDateStr,
                    endDate: endDateStr 
                },
                success: function (data) { 
                    console.log(data['startDate'] + ' ' + data['endDate']);
                    
                }
            }); 
        },
        selectOverlap: function(event) {
            if(event.rendering == 'background'){
                alert('Cannot Create at Block');
                location.reload();
            }
            console.log(event);
        }",
        'navLinkDayClick' => "
        function(date, jsEvent){
        	$('#modal-default').modal('toggle');
        }",
        'eventRender' => "function(event, element, view) {
            $(element).html((moment(event.start).format('h:mm'))+'-'+event.title).attr('title',event.title);
            $(element).tooltip();
            return element;
        }"
	        ]);
	        return $calendar;
	        //return view('calendar.createEvent', ['calendar' => $calendar]);
	}

	//Callback Ajax
	public function setRescheduleCallback(Request $req){
		$id = $req->id;
		$date = $req->date;
		M_Appointment::where('id',$id)->update([
			'reschedule_date' => $date,
			'status' => 'Waiting',
			'color' => '#cf70f4'
		]);
		return response()->json([
			'id' => $id,
			'date' => $date
		]);
	}
	public function setCancelCallback(Request $req){
		$id = $req->id;
		M_Appointment::where('id',$id)->update([
			'status' => 'Cancel',
			'color' => '#ea4f4f'
		]);
		return response()->json([
			'id' => $id,
			'mssg' => "Success Cancel"
		]);
	}
	public function setDenyCallback(Request $req){
		$id = $req->id;
		M_Appointment::where('id',$id)->update([
			'status' => 'Cancel',
			'color' => '#ea4f4f'
		]);
		return response()->json([
			'id' => $id,
			'mssg' => "Success Deny"
		]);
	}
	public function setAcceptCallback(Request $req){
		$id = $req->id;
		$data = M_Appointment::select('reschedule_date','id_user','to_user','name','description')->where('id',$id)->first();
		$splitDateTime = preg_split('/[T+]/', $data['reschedule_date']);
    	$replaceDate = $splitDateTime[0];
    	$dateformat = date("d/m/Y" , strtotime($replaceDate));
    	$startTime = $splitDateTime[1];
    	$endTime = date("H:i:s" , strtotime($startTime) + 60*15);
    	$startDate = $data['reschedule_date'];
    	$endDate = $replaceDate . "T" . $endTime . "+07:00";
        $rtoken = $req->session()->get('RToken');
		M_Appointment::where('id',$id)->update([
			'status' => 'Active',
			'color' => null,
			'start_date' => $startDate,
			'end_date' => $endDate,
			'reschedule_date' => null
		]);
		$this->storeCalendar($rtoken,$startDate,$endDate,$data['name'],$data['description']);
		// return Redirect::route('createapp.store')->with([

		// ]); 
		return response()->json([
			'Start Date' => $startDate,
			'End Date' => $endDate,
			'Name' => $data['name'],
			'Description' => $data['description'],
			'RToken' => $rtoken
		]);
		
	}
	public function storeCalendar($rtoken, $startDate,$endDate,$name,$description)
    {   
        $id_user = Auth::id();
        $getAtoken = Session::get('AToken');
        $getExipres_in = "3600";
        $getRtoken = $rtoken;
        $getScope = "https://www.googleapis.com/auth/calendar";
        $getStartDate = $startDate;
        $getEndDate = $endDate;;
        $this->setSession($getAtoken, $getExipres_in, $getRtoken, $getScope);

        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $this->client->setAccessToken($_SESSION['access_token']);
            $service = new Google_Service_Calendar($this->client);

            $calendarId = 'primary';
            $event = new Google_Service_Calendar_Event([
                'summary' => $name,
                'description' => $description,
                'start' => ['dateTime' => $getStartDate],
                'end' => ['dateTime' => $getEndDate],
                'reminders' => ['useDefault' => true],
            ]);
            $results = $service->events->insert($calendarId, $event);
            // if (!$results) {
            //     return response()->json(['status' => 'error', 'message' => 'Something went wrong']);
            // }else{
            //     return response()->json(['status' => 'success', 'message' => 'Event Created']);
            // }
        } else {
            return redirect()->route('oauthCallback');
        }
    }
}
