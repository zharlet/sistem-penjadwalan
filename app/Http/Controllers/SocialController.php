<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Validator,Redirect,Response,File;
use Socialite;
use Session;
use JavaScript;
use App\User;
use App\M_ManageSchedule;
class SocialController extends Controller
{
	public function redirectToProvider()
	{
	    return Socialite::driver('google')->redirect();
	}
	 
	public function handleProviderCallback()
	{     
		$getInfo = Socialite::driver('google')->user();
		if(Session::get('action_mail_update') != null){
			User::where('id',\Auth::id())->update([
				'email' => $getInfo->email
			]);
			Session::forget('action_mail_update');
			return redirect("/setting");
		}
		if(Session::get('action_password_reset') != null){
			$email = User::select("email")->get();
			$emailArray = [];
			foreach($email as $key => $value){
				$emailArray[] = $value->email;
			}
			if(in_array($getInfo->email, $emailArray)){
				Session::put('email_login',$getInfo->email);
				Session::forget('action_password_reset');
				return view('auth.passwords.reset');
			}else{
				JavaScript::put(['cobbaa' => "Google Account not registered"]);
				return redirect("/login");
			}
		}
	    return \Redirect::route('register',['name' => $getInfo->name , 'email' => $getInfo->email, 'photo' => $getInfo->avatar, 'token' => csrf_token()])->with('message', 'State saved!');
	}
	function createUser($getInfo,$provider){ 
		$user = User::where('provider_id', $getInfo->id)->first();
		 if (!$user) {
		     $user = User::create([
		        'name'     => $getInfo->name,
		        'email'    => $getInfo->email,
		        'provider' => $provider,
		        'provider_id' => $getInfo->id
		    ]);
		  }
		  return $user;
	}
}