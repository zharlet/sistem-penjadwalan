<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use App\User;
// use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    // use ResetsPasswords;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }
    public function index(Request $req){
        $email = $req->session()->get('email_login');
        $password = $req->password;
        $password_confirmed = $req->password_confirmation;
        $email = $req->session()->get('email_login');
        if(strcmp($password, $password_confirmed) != 0){
            return redirect()->back()->with('report',"Password Not Match");
        }
        $validate = $req->validate([
            'password' => 'required|string|min:6|confirmed'
        ]);
        User::where('email',$email)->update([
            'password' => bcrypt($password)
        ]);
        return redirect()->to('login');
    }
}
