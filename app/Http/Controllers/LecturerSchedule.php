<?php

namespace App\Http\Controllers;

use Auth;
use JavaScript;
use DateTime;
use App\M_Appointment;
use App\M_ManageSchedule;
use App\M_Preset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use MaddHatter\LaravelFullcalendar\Facades\Calendar;

class LecturerSchedule extends Controller
{
	function __construct(){
		$this->middleware('auth');
	}
    public function index(){
        $y = 0;
        $idPreset = "";
    	$theme = "";
    	$setting = null;
        $splitRoom = [];
        $jsSetting = [];
        $businessHours = [];
    	$businessHours2 = [];
        $getPresets = M_ManageSchedule::select('id_presets')->where('id_users',Auth::id())->first();
        $dataPresets = M_Preset::select('id','name','hiddenDays','minTime','maxTime','slotDuration','businessHours','rooms','themes')->where('id_users',Auth::id())->get()->toArray();
        $data = M_Preset::select('id','name', 'hiddenDays','minTime','maxTime','slotDuration','businessHours','rooms','themes')->where('id',$getPresets['id_presets'])->get()->toArray();
        if($getPresets['id_presets'] == null){
            $key['height'] = "700";
            $key['header'] = [
                'left' => 'prev,next today',
                'center' => 'title',
                'right' => 'agendaWeek',
            ];
            $setting = $key;
            $calendar = $this->fullCalendar($setting);
            return view('manageSchedule',['calendar'=>$calendar , 'pageSpec' => true]);
        }
    	foreach ($data as $key) {
            $idPreset = $key['id'];
    		$getKeyBH = $key['businessHours'];
    		$getKeyHD = $key['hiddenDays'];
            $theme = $key['themes'];
            $userPreset = $key['name'];
    		$spliting = preg_split('/[;-]/', $getKeyBH, null, PREG_SPLIT_NO_EMPTY);
    		$splitcomma = array_map('intval', preg_split('/[,]/', $getKeyHD, null, PREG_SPLIT_NO_EMPTY));
    		for($x=0; $x < count($spliting)/5; $x++){
                $temp = [
                    'overlap' => $spliting[$y],
                    'dow' => $spliting[$y+1],
                    'start' => $spliting[$y+2],
                    'end' => $spliting[$y+3],
                    'room' => $spliting[$y+4]
                ];
	            array_push($businessHours, $temp);
	            $y+=5;
	        }
	        $key['businessHours'] = $businessHours;
	        $key['hiddenDays'] = $splitcomma;
            $key['height'] = "700";
            $key['header'] = [
                'left' => 'prev,next today',
                'center' => 'title',
                'right' => 'agendaWeek',
            ];
	        $setting = $key;
    	}

        //for JS
        foreach ($dataPresets as $key) {
            $y = 0;
            $getKeyBH = $key['businessHours'];
            $getKeyHD = $key['hiddenDays'];
            $getKeyID = $key['id'];
            $getKeyRoom = preg_split('/[;-]/', $key['rooms'], null, PREG_SPLIT_NO_EMPTY);
            $spliting = preg_split('/[;-]/', $getKeyBH, null, PREG_SPLIT_NO_EMPTY);
            $splitcomma = array_map('intval', preg_split('/[,]/', $getKeyHD, null, PREG_SPLIT_NO_EMPTY));
            for($x=0;$x < count($getKeyRoom); $x+=2){
                if($getKeyRoom[$x] == "Sunday"){$valDay = 0;$splitRoom[$valDay] = $getKeyRoom[$x+1];}
                if($getKeyRoom[$x] == "Monday"){$valDay = 1;$splitRoom[$valDay] = $getKeyRoom[$x+1];}
                if($getKeyRoom[$x] == "Tuesday"){$valDay = 2;$splitRoom[$valDay] = $getKeyRoom[$x+1];}
                if($getKeyRoom[$x] == "Wednesday"){$valDay = 3;$splitRoom[$valDay] = $getKeyRoom[$x+1];}
                if($getKeyRoom[$x] == "Thursday"){$valDay = 4;$splitRoom[$valDay] = $getKeyRoom[$x+1];}
                if($getKeyRoom[$x] == "Friday"){$valDay = 5;$splitRoom[$valDay] = $getKeyRoom[$x+1];}
                if($getKeyRoom[$x] == "Saturday"){$valDay = 6;$splitRoom[$valDay] = $getKeyRoom[$x+1];}
            }
            for($x=0; $x < count($spliting)/5; $x++){
                $temp = [
                    'overlap' => $spliting[$y],
                    'dow' => $spliting[$y+1],
                    'start' => $spliting[$y+2],
                    'end' => $spliting[$y+3],
                    'room' => $spliting[$y+4]
                ];
                array_push($businessHours2, $temp);
                $y+=5;
            }
            $key['rooms'] = $splitRoom;
            $key['businessHours'] = $businessHours2;
            $key['hiddenDays'] = $splitcomma;
            $key['height'] = "700";
            $key['header'] = [
                'left' => 'prev,next today',
                'center' => 'title',
                'right' => 'agendaWeek',
            ];
            $jsSetting[$getKeyID] = $key;
            $businessHours2 = [];
        }
        JavaScript::put([
            'setting' => $setting,
            'theme' => $theme,
            'preset' => $jsSetting,
            'userPreset' => $userPreset
        ]);

    	$calendar = $this->fullCalendar($setting);
        if(\Session::get('report')!=null){
            $report = \Session::get('report');
            return view('manageSchedule',['setting'=>$setting , 'calendar'=>$calendar , 'pageSpec' => true , 'themeCalendar' => $theme, 'idPreset' => $idPreset, 'report' => $report ]);
        }
    	return view('manageSchedule',['setting'=>$setting , 'calendar'=>$calendar , 'pageSpec' => true , 'themeCalendar' => $theme, 'idPreset' => $idPreset ]);
    }
    public function create(Request $req){
        M_Preset::create([
            'id_users' =>  Auth::id(),
            'hiddenDays' => '0,6', 
            'name' => $req->newSchedule,
            'minTime' => "08:00",
            'maxTime' => "16:00",
            'slotDuration' => "00:30",
            'businessHours' => "-false;1,2,3,4,5;08:00;16:00;Default",
            'rooms' => "-Monday;Default-Tuesday;Default-Wednesday;Default-Thursday;Default-Friday;Default",
            'themes' => "flat"
        ]);
        return redirect()->back();
    }
    public function update(Request $req){

        $hiddenDays = "";
        $businessHours = "";
        //$timeBH = "";
        $section = "";
        $roomDay = "";

        //Worst Case
        if(!isset($req->day)){
            return redirect('/schedule/manage')->with('report','Please select at least 1 day to show');
        }
        if(DateTime::createFromFormat('H:i', $req->minTime) == false || DateTime::createFromFormat('H:i', $req->maxTime) == false ){
            return redirect('/schedule/manage')->with('report','Please insert time by format = H:i');
        }
        /*Worst Case*/

        if(!isset($req->day[0])){
             $hiddenDays = "0";
        }

        for($i=1;$i<7;$i++){
            if(isset($req->day[$i])){
                continue;
            }else{
                if($hiddenDays == ""){
                    $hiddenDays = $i;
                }
                else{
                    $hiddenDays .= "," . $i;
                }
            }
        }

        for($i=0;$i<=6;$i++){
            if(isset($req->time[$i]) && isset($req->day[$i])){
                $day = $req->day[$i];
                $room = $req->room[$i];
                if($i == 0){$roomDay .= "-Sunday;".$room;}
                if($i == 1){$roomDay .= "-Monday;".$room;}
                if($i == 2){$roomDay .= "-Tuesday;".$room;}
                if($i == 3){$roomDay .= "-Wednesday;".$room;}
                if($i == 4){$roomDay .= "-Thursday;".$room;}
                if($i == 5){$roomDay .= "-Friday;".$room;}
                if($i == 6){$roomDay .= "-Saturday;".$room;}

                
                if(strpos($req->time[$i],";") !== false){
                    $section = explode(";",$req->time[$i]);
                    foreach ($section as $value) {
                        $time = explode("-",$value);
                        if(DateTime::createFromFormat('H:i', $time[0]) == false){
                            return redirect('/schedule/manage')->with('report','Please insert time by format = H:i');
                        }
                       
                        if($i == 0){
                           $businessHours .= "-false;" . $day .";$time[0]" . ";$time[1]". ";$room";
                        }else{
                            $businessHours .= "-false;" . $day .";$time[0]" . ";$time[1]". ";$room" ;
                        }
                    }  
                }else{
                    $time = explode("-",$req->time[$i]);
                    if(DateTime::createFromFormat('H:i', $time[0]) == false){
                        return redirect('/schedule/manage')->with('report','Please insert time by format = H:i');
                    }
                     if($i == 0){
                        $businessHours .= "-false;" . $day .";$time[0]" . ";$time[1]" . ";$room";
                     }else{
                         $businessHours .= "-false;" . $day .";$time[0]" . ";$time[1]" . ";$room";
                     }
                }           
             }else{
                continue;
             }
             
        }

        $getIDPreset = $req->idPreset;
        M_Preset::where('id',$getIDPreset)->update([
            'hiddenDays' => $hiddenDays , 
            'name' => $req->namePreset,
            'minTime' => $req->minTime,
            'maxTime' => $req->maxTime,
            'slotDuration' => $req->slotDuration,
            'businessHours' => $businessHours,
            'rooms' => $roomDay,
            'themes' => $req->themesCalendar
        ]);
        M_ManageSchedule::where('id_users',Auth::id())->update([
            'id_presets' => $getIDPreset
        ]);
        return redirect('/schedule/manage');
    }
    public function fullCalendar($setting){
        $events = [];
        $setOption = [];

        $calendar = Calendar::addEvents($events)->setOptions($setting)->setCallbacks([
        'dayClick' => "
        function(date, jsEvent) {},
        select: function(startDate, endDate, jsEvent) {

        },
        selectOverlap: function(event) {
            if(event.rendering == 'background'){
                alert('Cannot Create at Block');
                location.reload();
            }
            console.log(event);
        },
        dayRender: function(date, cell) {
          cell.attr('MyCustomAttr', 'Day Of Year: ' + date.format('DDD'));
        }"
        ]);
        return $calendar;
        //return view('calendar.createEvent', ['calendar' => $calendar]);
    }
}
