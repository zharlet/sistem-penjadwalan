<?php

namespace App\Http\Controllers;

use Auth;
use JavaScript;
use Carbon\Carbon;
use Google_Client;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;
use Google_Service_Calendar_EventDateTime;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

use App\User;
use App\M_Appointment;
use App\M_Preset;
use App\M_ManageSchedule;
use App\M_TokenUser;
use App\Http\Requests;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;

class CreateAppointment extends Controller
{
    protected $client;
    
    public function __construct()
    {
        $client = new Google_Client();
        $client->setAuthConfig('client_secret.json');
        $client->addScope(Google_Service_Calendar::CALENDAR);
        $guzzleClient = new \GuzzleHttp\Client(array('curl' => array(CURLOPT_SSL_VERIFYPEER => false)));
        $client->setHttpClient($guzzleClient);
        $client->setAccessType('offline');
        $client->setApprovalPrompt('force');
        $this->client = $client;
        $this->middleware('auth', ['except']);
    
    }

    public function index()
    {
        session_start();
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $this->client->setAccessToken($_SESSION['access_token']);
            $service = new Google_Service_Calendar($this->client);
            $calendarId = 'primary';
            $results = $service->events->listEvents($calendarId);
            $unamesch =  User::select('username_sch')->where('id' , Auth::id())->first();
            try {
                if($unamesch['username_sch'] == null){
                    User::where('id' , Auth::id())->update([
                        'gcalendar' => $results->summary,
                        'username_sch' => $results->summary
                    ]);
                }else{
                    User::where('id' , Auth::id())->update([
                        'gcalendar' => $results->summary
                    ]);
                }
            } catch (\Illuminate\Database\QueryException $e) {
                $errorCode = $e->errorInfo[1];
                if($errorCode == 1062){
                    return redirect('setting')->with('report','Email has been use by other [Try again in 60 minutes]');
                }
            }

            $this->tokenDB();
            return redirect('setting');
        } else {
            return redirect()->route('oauthCallback');
        }
    }

    public function setSession($access_token, $expires_in, $refresh_token, $scope){
        session_start();
        $_SESSION['access_token'] = array(
            "access_token" => $access_token,
            "expires_in" => $expires_in,
            "refresh_token" => $refresh_token,
            "scope" => $scope,
        );
    }

    public function oauth()
    {
        session_start();
        $rurl = action('CreateAppointment@oauth');
        $this->client->setRedirectUri($rurl);
        if (!isset($_GET['code'])) {
            $auth_url = $this->client->createAuthUrl();
            $filtered_url = filter_var($auth_url, FILTER_SANITIZE_URL);
            return redirect($filtered_url);
        } else {
            $this->client->authenticate($_GET['code']);
            $_SESSION['access_token'] = $this->client->getAccessToken();
            return redirect()->route('createapp.index');
        }
    }

    public function create($username, Request $request)
    {
        session_start();
        $error = "";
        $theme = "";
        if($request->session()->get('error') != null){
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }
        $setting = null;
        $businessHours = [];
        $id_user = User::where('username_sch',$username)->get()->first()->toArray();

        if(!$id_user['isOnline']){
            return view('errors.offline');
        }
        
        $request->session()->put('to_user',$id_user);

        $userPreset = M_ManageSchedule::select('id_presets')->where('id_users',$id_user)->first();
        $data = M_Preset::select('hiddenDays','minTime','maxTime','slotDuration','businessHours','rooms','themes')->where('id',$userPreset['id_presets'])->get()->toArray();
        $y = 0;
        foreach ($data as $key) {
            $theme = $key['themes'];
            $getKeyBH = $key['businessHours'];
            $getKeyHD = $key['hiddenDays'];
            $spliting = preg_split('/[;-]/', $getKeyBH, null, PREG_SPLIT_NO_EMPTY);
            $splitcomma = array_map('intval', preg_split('/[,]/', $getKeyHD, null, PREG_SPLIT_NO_EMPTY));
            for($x=0; $x < count($spliting)/5; $x++){
            $temp = [
                'overlap' => $spliting[$y],
                'dow' => $spliting[$y+1],
                'start' => $spliting[$y+2],
                'end' => $spliting[$y+3],
            ];
                array_push($businessHours, $temp);
                $y+=5;
            }
            $key['businessHours'] = $businessHours;
            $key['hiddenDays'] = $splitcomma;
            $key['height'] = 500;
            $key['validRange'] = [
                'start' => date('Y-m-d',strtotime(date('Y-m-d')."+1 days")),
                'end' => date('Y-m-d',strtotime(date('Y-m-d')."+30 days"))
            ];
            $key['header'] = [
                'left' => 'prev',
                'center' => 'title',
                'right' => 'next'
            ];
            $key['firstDay'] = date('N', strtotime(date('D')."+1 days"));
            $setting = $key;
        }
       // dd($setting);
        $calendar = $this->fullCalendar($setting);
        $get_ID = User::select('id','name')->where('username_sch', $username)->first();
        $get_ID = json_decode(json_encode($get_ID),true);

        $get_Name = $get_ID['name'];
        $get_ID = $get_ID['id'];

        $get_RToken = M_TokenUser::select('refresh_token')->where('id_users', '=', $get_ID)->first();
        $get_RToken = json_decode(json_encode($get_RToken),true);
        $get_RToken = $get_RToken['refresh_token'];

        $rooms = $data[0]['rooms'];
        $roomsplit = preg_split('/[;-]/', $rooms, null, PREG_SPLIT_NO_EMPTY);
        
        $roomfix = [];
        for($x=0;$x<count($roomsplit);$x+=2){
            $temp = $roomsplit[$x];
            $roomfix[$temp] = $roomsplit[$x+1];
        }

        $request->session()->put('RToken',$get_RToken);
        Javascript::put([
            'presetInUse' => $userPreset['id_presets'],
            'rooms' => $roomfix
            
        ]);
        return view('calendar.createEvent',['refresh_token' => $get_RToken , 'name' => $get_Name , 'calendar' => $calendar , 'error' => $error , 'themeCalendar' => $theme, 'myname' => Auth::user()->name]);
    }
    public function store(Request $request)
    {   
        if($request->session()->get('startDate') != null){
            $id_user = Auth::id();
            $to_user = $request->session()->get('to_user');
            $getAtoken = $request->session()->get('AToken');
            $getExipres_in = "3600";
            $getRtoken = $request->session()->get('RToken');
            $getScope = "https://www.googleapis.com/auth/calendar";
            $getStartDate = $request->session()->get('startDate');
            $getEndDate = $request->session()->get('endDate');;
            $this->setSession($getAtoken, $getExipres_in, $getRtoken, $getScope);

            if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
                $this->client->setAccessToken($_SESSION['access_token']);
                $service = new Google_Service_Calendar($this->client);

                $calendarId = 'primary';
                $event = new Google_Service_Calendar_Event([
                    'summary' => $request->title,
                    'description' => $request->description . " " . $request->information,
                    'start' => ['dateTime' => $getStartDate],
                    'end' => ['dateTime' => $getEndDate],
                    'reminders' => ['useDefault' => true],
                ]);
                $results = $service->events->insert($calendarId, $event);
                if (!$results) {
                    return response()->json(['status' => 'error', 'message' => 'Something went wrong']);
                }else{
                    M_Appointment::create([
                        'name' => $request->title,
                        'description' => $request->description . " " . $request->information,
                        'place' => $request->place,
                        'id_user' => $id_user,
                        'to_user' => $to_user['id'],
                        'start_date' => $getStartDate,
                        'end_date' => $getEndDate,
                        'status' => "Active"
                    ]);
                    $request->session()->forget('startDate');
                    return redirect('/appointment/list');
                    //return response()->json(['status' => 'success', 'message' => 'Event Created']);
                }
                
            } else {
                return redirect()->route('oauthCallback');
            }
        }else{
            return back()->with('error','please select date');
        }
        
    }

    public function show($eventId)
    {
        session_start();
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $this->client->setAccessToken($_SESSION['access_token']);

            $service = new Google_Service_Calendar($this->client);
            $event = $service->events->get('primary', $eventId);

            if (!$event) {
                return response()->json(['status' => 'error', 'message' => 'Something went wrong']);
            }
            return response()->json(['status' => 'success', 'data' => $event]);

        } else {
            return redirect()->route('oauthCallback');
        }
    }
    public function edit($id)
    {
        //
    }
    public function update(Request $request, $eventId)
    {
        session_start();
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $this->client->setAccessToken($_SESSION['access_token']);
            $service = new Google_Service_Calendar($this->client);
            $startDateTime = Carbon::parse($request->start_date)->toRfc3339String();
            $eventDuration = 30; //minutes

            if ($request->has('end_date')) {
                $endDateTime = Carbon::parse($request->end_date)->toRfc3339String();

            } else {
                $endDateTime = Carbon::parse($request->start_date)->addMinutes($eventDuration)->toRfc3339String();
            }
            // retrieve the event from the API.
            $event = $service->events->get('primary', $eventId);
            $event->setSummary($request->title);
            $event->setDescription($request->description);

            //start time
            $start = new Google_Service_Calendar_EventDateTime();
            $start->setDateTime($startDateTime);
            $event->setStart($start);

            //end time
            $end = new Google_Service_Calendar_EventDateTime();
            $end->setDateTime($endDateTime);
            $event->setEnd($end);

            $updatedEvent = $service->events->update('primary', $event->getId(), $event);
            if (!$updatedEvent) {
                return response()->json(['status' => 'error', 'message' => 'Something went wrong']);
            }
            return response()->json(['status' => 'success', 'data' => $updatedEvent]);

        } else {
            return redirect()->route('oauthCallback');
        }
    }

    public function destroy($eventId)
    {
        session_start();
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $this->client->setAccessToken($_SESSION['access_token']);
            $service = new Google_Service_Calendar($this->client);
            $service->events->delete('primary', $eventId);
        } else {
            return redirect()->route('oauthCallback');
        }
    }
    public function tokenDB(){

        $access_token = $_SESSION['access_token']['access_token'];
        $scope = $_SESSION['access_token']['scope'];
        $refresh_token = $_SESSION['access_token']['refresh_token']; 
        $id_login = Auth::user()->id;
        M_TokenUser::where('id_users', $id_login)
            ->update(
            ['access_token' => $access_token, 'scope' => $scope, 'refresh_token' => $refresh_token ]
        );
    }
    public function fullCalendar($setting){
        $events = [];
        $eventUser = \Session::get('to_user')['id'];
        $data = M_Appointment::where('to_user',$eventUser)->get();
        if($data->count()) {
            foreach ($data as $key => $value) {
                // $events[] = array(
                //     'id' => null,
                //     'title' => $value->title,
                //     'allDay' => false,
                //     'start'=> $value->start_date,
                //     'end'=> $value->end,
                //     'overlap'=>$value->overlap,
                //     'rendering'=>$value->render,
                //     'background'=>$value->color
                // );
                $events[] = Calendar::event(
                    $value->title,
                    false,
                    $value->start_date,
                    $value->end_date,
                    null,
                    // Add color and link on event
                 [
                    'overlap' => $value->overlap,
                    'rendering' => $value->render,
                    'color' => $value->color,
                 ]
                );

            }
        }
        //dd($events);
        // $events[] = Calendar::event(
        //             "Non businessHours",
        //             false,
        //             "2019-04-29T10:00:00",
        //             "2019-04-30T10:00:00",
        //             null,
        //             // Add color and link on event
        //          [
        //             'overlap' => false,
        //             'rendering' => "background",
        //             'color' => '#ff9f89',
        //          ]
        //         );
        // $events[] = Calendar::event(
        //             "Non businessHours",
        //             false,
        //             "10:00:00",
        //             "14:00:00",
        //             null,
        //             // Add color and link on event
        //          [
        //             'dow'=>[6],
        //             'overlap' => false,
        //             'rendering' => "background",
        //             'color' => '#ff9f89',
        //          ]
        //         );

        $calendar = Calendar::addEvents($events)->setOptions($setting)->setCallbacks([
        'dayClick' => "
        function(date, jsEvent) {},
        select: function(startDate, endDate, jsEvent) {
          var startDateStr = startDate.format();
          var dayStr = startDate.format('dddd');
          var endDateStr = endDate.format();
          var CSRF_TOKEN = $('meta[name=\"\csrf-token\"\]').attr('content');
          var humanSelection = true;
          $.ajax({
                url: '/make/appointment/',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN, 
                    startDate: startDateStr,
                    endDate: endDateStr,
                    presetInUse: myval.presetInUse,
                    day: dayStr 
                },
                success: function (data) { 
                    console.log(data['startDate'] + ' ' + data['endDate']);
                    $('input[name=\"\place\"\]').val(data['room']);
                }
            }); 
        },
        selectOverlap: function(event) {
            return false;
        },
        selectAllow: function(selectInfo) {
          return moment().diff(selectInfo.start) <= 0
        }",
        'eventRender'=>"function(event,element){
            $(element).html('');

        }"
        ]);
        return $calendar;
        //return view('calendar.createEvent', ['calendar' => $calendar]);
    }
}
