<?php

namespace App\Http\Controllers\ajax;

use Illuminate\Http\Request;
use App\M_Appointment;
use App\M_Preset;
use App\Http\Controllers\Controller;

class setAppointment extends Controller
{
    public function index(Request $req){
    	// Schedule::create(array(
    	// 	'title' => $req->desc,
    	// 	'start_date' => $req->startDate,
    	// 	'end_date' => $req->endDate
    	// ));
        $rooms = [];
        $room = "";
        $roomsData = M_Preset::select('rooms')->where('id',$req->presetInUse)->first();
        $spliting = preg_split('/[;-]/', $roomsData['rooms'], null, PREG_SPLIT_NO_EMPTY);
        for($x=0;$x<count($spliting);$x+=2){
            $temp = $spliting[$x];
            if($req->day == $temp){
                $room = $spliting[$x+1];
            }
        }
    	$req->session()->put([
    		'startDate'=> $req->startDate . "+07:00" ,
    		'endDate' => $req->endDate . "+07:00"
    	]);
    	return response()->json([
    		'startDate' => $req->session()->get('startDate'),
    		'endDate' => $req->session()->get('endDate'),
            'room' => $room
    	]);
    }
}
