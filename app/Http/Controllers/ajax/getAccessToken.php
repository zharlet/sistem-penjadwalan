<?php

namespace App\Http\Controllers\ajax;

use App\M_TokenUser;
use App\M_Appointment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class getAccessToken extends Controller
{
	public function index(Request $request){
		$ch = curl_init();
		$auth_url = "https://www.googleapis.com/oauth2/v4/token";
		if(isset($request->action)){
			$data = M_Appointment::select('to_user')->where('id',$request->id)->first();
			$getRToken = M_TokenUser::select('refresh_token')->where('id_users', $data['to_user'])->first();
			$request->session()->put('RToken',$getRToken['refresh_token']);
		}

		$post = [
		    'client_id' => '276645278122-njmsnjj6m57849t4me70hd1lcmps7lj0.apps.googleusercontent.com',
		    'client_secret' => 'WXiMQQfV0DHX02jlivUggTVp',
		    'refresh_token' => $request->session()->get('RToken'),
		    'grant_type'   => 'refresh_token',
		];
		$postText = http_build_query($post);
		$options = array(
		    CURLOPT_URL =>$auth_url,
		    CURLOPT_POST => true,
		    CURLOPT_SSL_VERIFYPEER =>false,
		    CURLOPT_RETURNTRANSFER =>true,
		    CURLOPT_POSTFIELDS => $postText
		);

		curl_setopt_array($ch, $options);

		$response = curl_exec($ch);

		curl_close($ch);


		$json = json_decode($response);

		$request->session()->put('AToken',$json->access_token);
		
		return response()->json($request->session()->get('AToken'));
		//return response()->json($json);
	}

}
