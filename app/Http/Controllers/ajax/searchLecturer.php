<?php

namespace App\Http\Controllers\ajax;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class searchLecturer extends Controller
{
    public function index(Request $req){
    	$uname_sch = $req->get('uname_sch');
    	$isAvailable = User::where('username_sch',$uname_sch)->first();
        $photo = $isAvailable['photo'] != null ? $isAvailable['photo'] : "#";
    	if($isAvailable != null){
    		$name = $isAvailable['name'];
    		return response()->json([
    			'status' => "Username Available",
    			'name' => $name,
                'photo' => $photo
    		]);
    	}else{
    		return response()->json([
    			'status' => "Not Found"
    		]);
    	}
    	
    }
}
