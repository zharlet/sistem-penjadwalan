<?php

namespace App\Http\Controllers;

use Auth;
use JavaScript;
use DateTime;
use App\M_Appointment;
use App\M_ManageSchedule;
use App\User;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $_SESSION['login'] = "true";
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $check_manage_schedule = M_ManageSchedule::where('id_users',Auth::id())->first();
        if($check_manage_schedule['id_users'] == null){
            M_ManageSchedule::create([
                'id_users' => Auth::id(),
                'hiddenDays' => null,
                'minTime' => "07:00:00",
                'maxTime' => "18:00:00",
                'slotDuration'=> "00:15",
                'businessHours' => "-false;0,1,2,3,4,5,6;07:00;18:00"
            ]);
        }

        if(Auth::user()->user_unique == "Mahasiswa"){
            return $this->homeStudent();
        }else{
            return $this->homeLecturer();
        }
    }
    public function homeLecturer(){
        $activeApp = [];
        $checkAppointment = M_Appointment::where('id_user',Auth::id())->first();
        $appointment = M_Appointment::where('to_user',Auth::id())->get();
        $photo = User::where('id', Auth::id())->first();
        $isFirst = User::select('gcalendar')->where('id', Auth::id())->first();

        if($isFirst->gcalendar)
            $isFirst = 0;
        else
            $isFirst = 1;

        Javascript::put([
            'isFirst' => $isFirst
        ]);
        
        if($appointment != null){
            $totalApp = count($appointment);
            foreach($appointment as $key => $value){
                if($value->status == "Active"){
                    $activeApp[] = 1;
                }
            }
            $totalActiveApp = count($activeApp);
            $schedule = M_ManageSchedule::select('updated_at')->where('id_users',Auth::id())->first();
            $lastUpdate = $schedule['updated_at']->format('d M Y, H:i');

            $detail = [
                'totalApp' => $totalApp,
                'lastUpdate' => $lastUpdate,
                'totalActiveApp' => $totalActiveApp,
                'photo' => $photo['photo']
            ];

            return view('frontpage', ['user_unique' => Auth::user()->user_unique , 'detail' => $detail]);
        }else{
            $detail = [
                'totalApp' => 0,
                'lastUpdate' => "No Update",
                'totalActiveApp' => "No Appointment",
                'photo' => $photo['photo']
            ];

            return view('frontpage', ['user_unique' => Auth::user()->user_unique , 'detail' => $detail]);
        }
        
    }
    public function homeStudent(){
        $dateformat = [];
        $currentDate = new DateTime(date("m/d/Y H:i:s"));
        $latestDate = $currentDate;
        $nonActiveApp = [];
        $user_id = Auth::user()->id;
        $user_unique = User::where('id', $user_id)->first();
        $appointment = M_Appointment::where('id_user',Auth::id())->get();
        if($appointment != null){
            $totalApp = count($appointment);
            foreach ($appointment as $key => $value){
                if($value->status == "Active"){
                    $splitDateTime = explode("T", $value->start_date);
                    $replaceDate = str_replace("-", "/", $splitDateTime[0]);
                    $fixdate = $replaceDate . " " . substr($splitDateTime[1], 0, strpos($splitDateTime[1], "+"));
                    $dateformat[] = new DateTime(date("m/d/Y H:i:s" , strtotime($fixdate)));
                }
                
            }
            foreach($dateformat as $value){
                if($value > $latestDate){
                    $latestDate = $value;
                }
            }
            $latestApp = $latestDate->format('d M Y, H:i');
            foreach ($appointment as $key => $value){
                if($value->status != "Active"){
                    $nonActiveApp[] = $value->status;
                }
            }
            $totalNonActiveApp = count($nonActiveApp);

            $detail = [
                'totalApp' => $totalApp,
                'latestApp' => $latestApp,
                'totalNonActiveApp' => $totalNonActiveApp,
                'photo' => $user_unique->photo
            ];
            
            return view('frontpage', ['user_unique' => $user_unique->user_unique , 'detail' => $detail]);
        }else{
             $detail = [
                'totalApp' => 0,
                'latestApp' => "No Appointment",
                'totalNonActiveApp' => 0,
                'photo' => $user_unique->photo
            ];
            return view('frontpage', ['user_unique' => $user_unique->user_unique , 'detail' => $detail]);
        }
        
    }
}
