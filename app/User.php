<?php

namespace App;

use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','user_unique' , 'username_sch' , 'gcalendar' , 'isOnline' , 'photo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function notebooks()
    {
        return $this->hasMany(Notebook::class);
    }
    
    public function readUnique(){
        $user_id = Auth::user()->id;
        $user_unique = DB::table('users')->where('user_unique', '=', $user_id)->first();
        return $user_unique;
    }
}
