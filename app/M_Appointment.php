<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_Appointment extends Model
{
	public $table = "appointments";
    protected $fillable = ['place','description','start_date','end_date','reschedule_date','id_user','color','to_user','name' ,'status'];
}
