<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManageSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manage_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_users');
            $table->string('hiddenDays');
            $table->string('minTime');
            $table->string('maxTime');
            $table->string('slotDuration');
            $table->text('businessHours');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manage_schedule');
    }
}
